app.controller('convertCtrl', ['$scope', function ($scope) {
    // Define $scope.telephone as an array
    $scope.csvType = [];
    $scope.jsonType = [];
    // Create a counter to keep track of the additional telephone inputs
    $scope.inputCounter = 0;

    // This is just so you can see the array values changing and working! Check your console as you're typing in the inputs :)
    $scope.$watch('csvType', function (value) {
        console.log(value);
    }, true);
    $scope.$watch('jsonType', function (value) {
        console.log(value);
    }, true);



    $scope.getAllCSVJsonSettings = function(){
    	var settings = localStorage.getItem("convertOptions");
    	var jsonObj = JSON.parse(settings);
    	var csvOptions = jsonObj.csvOptions;
    	var jsonOptions = jsonObj.jsonOptions;

    	var allSet = [];
    	
    	for(var i=0;i<csvOptions.length;i++){
    		 var jcsv = {};
    		 jcsv.csv = csvOptions[i];
    		 jcsv.json = jsonOptions[i];

    		 allSet.push(jcsv);
    	}

    	$scope.csvJsonSettings = allSet;
/**

"abc": [
			{},{}
     ]

*/

    	for(var j=0;j<jsonOptions.length;j++){
    		if(jsonOptions[j]){

    		}
    		else{

    		}
    	}



    }


    $scope.saveOptions = function(){
    	var data = {"csvOptions":$scope.csvType, "jsonOptions": $scope.jsonType};
    	localStorage.setItem("convertOptions",JSON.stringify(data));
    	$scope.getAllCSVJsonSettings(); 
    }

    var csvJsonSettings = "#csvJsonSettings";
    $scope.$watch(function() { return angular.element(csvJsonSettings).is(':visible') }, function() {
        if(angular.element(csvJsonSettings).is(':visible') != true){
            return false;
        }
       
       $scope.getAllCSVJsonSettings(); 

    });



}]);


app.directive('addInput', ['$compile', function ($compile) { // inject $compile service as dependency
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            // click on the button to add new input field
            element.find('button').bind('click', function () {
                // I'm using Angular syntax. Using jQuery will have the same effect
                // Create input element

var input = angular.element(`<div class="form-group">
		  <div class="col-sm-6"><label for="csv">CSV:</label>
		  <input ng-model="csvType[` + scope.inputCounter + `]" type="text" class="form-control" ></div>
		  <div class="col-sm-6"><label for="json">Json:</label>
		  <input ng-model="jsonType[` + scope.inputCounter + `]" type="text" class="form-control" ></div>
	  </div>`);


                // Compile the HTML and assign to scope
                var compile = $compile(input)(scope);

                // Append input to div
               element.append(input);

                // Increment the counter for the next input to be added
                scope.inputCounter++;
            });
        }
    }
}]);