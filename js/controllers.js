app.controller('loginCtrl', function(Base64, $scope, $http, $localStorage) {
    $scope.smsg = $scope.errmsg = null;
    $scope.login = function() {
        authdata = Base64.encode("clientapp" + ':' + "123456");
        console.log(authdata);
        var form = new FormData();
        form.append("grant_type", "password");
        form.append("username", $scope.username);
        form.append("password", $scope.password);
        form.append("scope", "read write");
        form.append("client_secret", "123456");
        form.append("client_id", "clientapp");
        showLoader(true);
        $http({
            method: 'POST',
            url: baseUrl + "/oauth/token",
            data: form,
            headers: {
                'Content-Type': undefined,
                "Authorization": "Basic " + authdata
            },
            transformRequest: angular.identity
        }).success(function(response) {
            console.log(response);
            $localStorage.username = $scope.username;
            $localStorage.password = $scope.password;
            $localStorage.token = response.access_token;

            console.log($scope);
            $scope.userdata($scope.username, $scope.password)
            showLoader(false);
        }).error(function(response) {
            console.log(response);
            $scope.errmsg = "Error! " + response.error_description;
            showLoader(false);
        });
    };

    //get user data
    $scope.userdata = function(email, psd) {
        console.log("fteching");


        $http({
            method: 'POST',
            url: baseUrl + "/login/getLoginUser?email=" + email + "&password=" + psd,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {

                $scope.errmsg = "Error! " + response.message;
                /*$.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });*/
            } else {
                console.log(response.data);
                $localStorage.user = response.data;
                $localStorage.user = response.data;
                if (response.data.companyName == null && response.data.companyAddress == null && response.data.website == null && response.data.theyDo == null && response.data.needDeliver == null && response.data.pickUpLocation == null) {
                    $localStorage.initialApp = true;
                    location.href = "company-profile.html";
                } else if (response.data.roles[0].id == 2) {
                    location.href = "adminliveOrders.html";
                } else {
                    location.href = "liveOrders.html";
                }


                /*$.sweetModal({
                    content: "Success",
                    icon: $.sweetModal.ICON_SUCCESS
                });*/
            }
        }).error(function(response) {
            console.log(response);
            $scope.errmsg = "Error! " + response.message;
            /*$.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });*/
        });
    };
});

app.controller('headerCtrl', function($scope, $http, $localStorage,$interval) {


    $scope.user = $localStorage.user;
    if ($scope.user == null) {
        location.href = "login.html";
    }

    $scope.logout = function() {
        $localStorage.$reset();
        location.href = "login.html";

    };
   
    

    $scope.getProfile = function() {

        location.href = "company-profile.html";

    };
    //$scope.checkAdmin()
    var path = window.location.href.split("/");
    $scope.crFile = path[path.length - 1];
    console.log("Cr File : " + $scope.crFile);
    $scope.menuList = [];

    $scope.getGmapKey = function(){
        var url = baseUrl + "/api/v1/order/gmap-key";
        console.log(url);
        $http({
            method: 'GET',
            url: url,
            headers: {
                "Content-Type": "application/json",
                // "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            $scope.gmapKey = response.key;
            $scope.checkPreOrdersInstant();

        });
    }
    

    $scope.generateLatLng = function(booking){

         console.log("I am here");

    if(booking.zoneId > 0 && booking.latitude != 0){
        console.log(booking);
        console.log("I am here inside booking");
        $scope.updateOrders(booking);
        return false;
    }
    var address = booking.addressLine1+booking.addressLine2;
    var uid = booking.user.id;
    var url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key='+$scope.gmapKey+'&components=country:ID';
    console.log(url);
    fetch(url)
    .then(function (response) {
        return response.json();
    })
    .then(function (data) {
        address = btoa(address);

        $http({
            method: 'POST',
            url: baseUrl + "/booking/getgeoloc/"+uid+"/"+address,
            data: data,
            headers: {
                "Content-Type": "application/json"              
            }
        }).success(function(response) {
            
            
            if(response.postalCode != ""){
                booking.postalCode = response.postalCode;
                booking.kecematan = response.kecematan;
                booking.kelurahan = response.kelurahan;
                booking.latitude = response.latitude;
                booking.longitude = response.longitude;
                booking.kota = response.kota;
                booking.provinci = response.provinci;
                booking.zoneId = response.zoneId;
                
                $scope.updatePreBooking(booking);
            }
            else{

                if(response.latitude != ""){
                    var latlng = response.latitude + "," + response.longitude;

                    var url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+latlng+'&key='+$scope.gmapKey;
                    
                    fetch(url)
                    .then(function (response) {
                        return response.json();
                    })

                    .then(function (data) {
                        console.log(url);
                        console.log(data);

                        address = btoa(address);

                        $http({
                            method: 'POST',
                            url: baseUrl + "/booking/getgeolocbylatlng/"+uid+"/"+address,
                            data: data,
                            headers: {
                                "Content-Type": "application/json"              
                            }
                        })
                        .success(function(response) {

                            console.log("response");
                            console.log(response);

                            if(response.postalCode != ""){
                                booking.postalCode = response.postalCode;
                                booking.kecematan = response.kecematan;
                                booking.kelurahan = response.kelurahan;
                                booking.latitude = response.latitude;
                                booking.longitude = response.longitude;
                                booking.kota = response.kota;
                                booking.provinci = response.provinci;
                                booking.zoneId = response.zoneId;
                                
                                $scope.updatePreBooking(booking);
                            }

                        })
                        .error(function(response) {
            
                        });

                    })
                    .catch(function (err) {
                        console.log("Something went wrong!", err);
                    });


            }

            }

            
        }).error(function(response) {
            
        });
        
    })
    .catch(function (err) {
        console.log("Something went wrong!", err);
    });
    
    
}

$scope.updatePreBooking = function(booking){
    console.log(booking);
    if(booking.postalCode == "" || booking.latitude == "" || booking.longitude == "" || booking.addressLine1 == "" || booking.bookingId == ""){
        return false;
    }
    if(booking.postalCode == 0 || booking.latitude == 0) return false;
    
    
    $http({
        method: 'POST',
        url: baseUrl + "/booking/update-preorders",
        data: booking,
        headers: {
            "Content-Type": "application/json"              
        }
    }).success(function(response) {
        
        $scope.updateOrders(booking);
        
    }).error(function(response) {
       
    });
    
}

$scope.updateOrders = function(booking){
    var bookings = [];
   
    if(booking.zoneId == 0 || booking.postalCode == 0 ) return false;
    if(booking.pickUpLocation.pickUpLatitude == null || booking.pickUpLocation.pickUpLatitude == 0 ) return false;
    bookings.push(booking);

     console.log(bookings);
    $http({
        method: 'POST',
        url: baseUrl + "/booking/update-bookings",
        data: bookings,
        headers: {
            "Content-Type": "application/json"              
        }
    }).success(function(response) {
        
        console.log(response);
        
    }).error(function(response) {
       
       console.log(response);
    });
}




    if ($scope.user.roles[0].id == 2) {

        $scope.menuList.push({
            "name": "Settings",
            "url": "settings.html",
            "icon": "fa fa-dashboard",
            "active": ($scope.crFile.includes("settings.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });
        
        $scope.menuList.push({
            "name": "Live Orders",
            "url": "adminliveOrders.html",
            "icon": "fa fa-dashboard",
            "active": ($scope.crFile.includes("liveOrders.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });
        $scope.menuList.push({
            "name": "Delivery Statistics",
            "url": "admindashborad.html",
            "icon": "fa fa-dashboard",
            "active": ($scope.crFile.includes("dashboard.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });
        $scope.menuList.push({
            "name": "Add Appointment Slot",
            "url": "appointment-slots.html",
            "icon": "fa fa-dashboard",
            "active": ($scope.crFile.includes("appointment-slots.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });
        $scope.menuList.push({
            "name": "Create a Zone",
            "url": "create-zone.html",
            "icon": "fa fa-dashboard",
            "active": ($scope.crFile.includes("create-zone.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });
        

        $scope.menuList.push({
            "name": "Generated Route",
            "url": "generatedRoute.html",
            "icon": "fa fa-dashboard",
            "active": ($scope.crFile.includes("generatedRoute.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });
        $scope.menuList.push({
            "name": "Update Custom Package",
            "url": "updatePackage.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("updatePackage.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });
        $scope.menuList.push({
            "name": "Set Vehicle Availability",
            "url": "availableVehicle.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("availableVehicle.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });
        $scope.menuList.push({
            "name": "User List",
            "url": "userlist.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("userlist.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });

        $scope.menuList.push({
            "name": "Drivers",
            "url": "driver-list.html",
            "icon": "fa fa-taxi",
            "active": ($scope.crFile.includes("driver-list.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });

        $scope.menuList.push({
            "name": "Scanners",
            "url": "scanners.html",
            "icon": "fa fa-taxi",
            "active": ($scope.crFile.includes("scanners.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });
        
        $scope.menuList.push({
            "name": "Regenerate ETA Points",
            "url": "regenerateRoute.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("regenerateRoute.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });

        $scope.menuList.push({
            "name": "Check Pre Orders",
            "url": "check-orders.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("check-orders.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });

        $scope.menuList.push({
            "name": "Order Status",
            "url": "orders.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("orders.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });


        $scope.checkPreOrdersInstant = function(){
            
          
                var url = baseUrl + "/api/v1/order/preorderlist";
                console.log(url);
                $http.get(url)
                   .then(function(response){
                       console.log("Preorder instant check");
                       for(var i=0;i<response.data.length;i++){
                        $scope.generateLatLng(response.data[i]);
                       }
                       
                   }).finally(function(){
                        //$scope.checkPreOrders();
                   });
            
        };

        //$scope.checkPreOrdersInstant();
        $scope.getGmapKey();

        // $scope.checkPreOrders = function(){
            
        //     $interval(function () {
                
                
        //         $scope.checkPreOrdersInstant();
                
                
        //       }, 120000);
            
        // };
        
        
        
        


    }
    if ($scope.user.roles[0].id == 1) {
        $scope.menuList.push({
            "name": "Live Orders",
            "url": "liveOrders.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("liveOrders.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });
        $scope.menuList.push({
            "name": "Delivery Statistics",
            "url": "dashboard.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("dashboard.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });
        $scope.menuList.push({
            "name": "Place an Order",
            "url": "advanced.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("advanced.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });
        $scope.menuList.push({
            "name": "Place Multiple Orders",
            "url": "multipleBooking.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("multipleBooking.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });

        $scope.menuList.push({
            "name": "Excel Upload",
            "url": "uploadfile.html",
            "icon": "fa fa-files-o",
            "active": ($scope.crFile.includes("uploadfile.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });

         $scope.menuList.push({
            "name": "Order Status",
            "url": "orders.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("orders.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });

    }
    
    if ($scope.user.roles[0].id == 4) {
    	$scope.menuList.push({
            "name": "Transporter",
            "url": "transporter.html",
            "icon": "fa fa-files-o",
            "active": ($scope.crFile.includes("transporter.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });
    }



});

app.controller('transportCtrl', function($scope, $http, $localStorage){
	
	$scope.getAllUserList = function() {
	    showLoader(true);
	    console.log("get All User");
	    $http({
	        method: 'GET',
	        url: baseUrl + "/api/businessList",
	       
	    }).success(function(response) {
	        console.log(response);
	        $scope.userList = response;
	        showLoader(false);
	        
	    }).error(function(response) {
	        showLoader(false);
	        console.log(response);
	       
	    });
	};
	
	$scope.getAllUserList();
    $scope.ids = [];
    $scope.allBookingSt = true;
    $scope.bookingListBook = [];
    $scope.toggleBookingsInTransporter = function(bookingList){
        $scope.allBookingSt = !$scope.allBookingSt;

        var x = localStorage.getItem("boolingList");
        var y = JSON.parse(x);

        for(var i=0; i<y.length;i++){
            $scope.bookingList[i].id = ($scope.allBookingSt)?y[i].id:0;
            console.log($scope.bookingList[i]);
        }

        
    }
	
	$scope.getBulkOrdersByUid = function(uid){
		showLoader(true);
		$http({
	        method: 'GET',
	        url: baseUrl + "/booking/get-bulk-order/"+uid,
	       
	    }).success(function(response) {
	        console.log(response);
	        $scope.bulkOrderList = response;
	        showLoader(false);
	        
	    }).error(function(response) {
	        showLoader(false);
	        console.log(response);
	       
	    });
		
	}
	
	$scope.getBulkOrdersByPickUp = function(uid,pickUp,st){
		$scope.OrderSt = st;
		$scope.pickUp = pickUp;
        var pickUpDate = angular.element(document.querySelector("#dateValue")).val();
        if(pickUpDate == ""){
            alert("Select a valid pickup date");
            return false;
        }
		showLoader(true);
		$http({
	        method: 'GET',
	        url: baseUrl + "/booking/get-bulk-order/"+uid+"/"+pickUp+"/"+st+"/"+pickUpDate,
	       
	    }).success(function(response) {
	        console.log(response);
	        $scope.bookingList = response;
            localStorage.setItem("boolingList", JSON.stringify(response));
	        $scope.selPickUp = pickUp;
	        showLoader(false);
	        
	    }).error(function(response) {
	        showLoader(false);
	        console.log(response);
	       
	    });
		
	}
	
	$scope.getPickUpLocationsByUid = function(uid){
		showLoader(true);
		$http({
	        method: 'GET',
	        url: baseUrl + "/booking/get-pickup-location/"+uid,
	       
	    }).success(function(response) {
	        console.log(response);
	        $scope.pickUps = response;
	        showLoader(false);
	        
	    }).error(function(response) {
	        showLoader(false);
	        console.log(response);
	       
	    });
		
	}
	
	$scope.changeBulkOrderStatus = function(bol,st){
		if(!confirm("Are you sure?")) return false;
		showLoader(true);
		$http({
	        method: 'GET',
	        url: baseUrl + "/booking/change-bulk-order-status/"+bol.bulkId+"/"+st,
	       
	    }).success(function(response) {
	        showLoader(false);
	        if(response.status == "success"){
	        	$scope.getBulkOrdersByUid($scope.userlist);
	        }
	        else{
		        $.sweetModal({
	                content: "Error!" + response.message,
	                icon: $.sweetModal.ICON_ERROR
	            });
	        }
	        
	        
	        
	    }).error(function(response) {
	        showLoader(false);
	        
	        $.sweetModal({
                content: "Error! Something went wrong",
                icon: $.sweetModal.ICON_ERROR
            });
	       
	    });
	}
	
	
	
	
	$scope.changePickUpStatusByBookings = function(bookings,st){
		
        $scope.allBookingSt = true;

        if(st == 1){
			if(!confirm("Are you sure, You want to pickup orders?")) return false;
		}
		if(st == 2){
			if(!confirm("Are you sure, You delivered orders to the hub?")) return false;
		}
		
		var book = bookings.filter(function(booking){
			
			return booking.id > 0;
		});
		
		console.log(book);
		
		
		showLoader(true);
		$http({
	        method: 'POST',
	        url: baseUrl + "/booking/update-bookings-pickup-status/"+st,
	        data: book,
	        headers: {
	            "Content-Type": "application/json"              
	        }
	    }).success(function(response) {
	    	showLoader(false);
	    	console.log(response);
	    	
	    	$scope.getBulkOrdersByPickUp($scope.userlist,$scope.pickUp,st);
	        
	    }).error(function(response) {
	    	showLoader(false);
	    	
	       
	    });
	}
	
	
	
	

	
});


// profile controller
app.controller('profileCtrl', function($scope, $http, $localStorage) {

    $scope.user = $localStorage.user;



    $scope.updateUser = function(user) {

        $http({
            method: 'POST',
            url: baseUrl + "/login/updateUser",
            data: user,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            //console.log(response);
            if (response.status == "Error") {
                // $scope.errmsg = "This user already exist";
                $.sweetModal({
                    content: "Error!" + response.message,
                    icon: $.sweetModal.ICON_ERROR
                });

            } else {
                //$scope.smsg = response.message;
                $localStorage.user = response.data;
                if ($localStorage.initialApp == true) {
                    location.href = "addPickUpLocation.html";
                }
                $.sweetModal({
                    content: "Company Profile Updated",
                    icon: $.sweetModal.ICON_SUCCESS
                });

            }
        }).error(function(response) {
            //console.log(response);
            // $scope.errmsg = response.message;
            $.sweetModal({
                content: "Error!" + response.message,
                icon: $.sweetModal.ICON_ERROR
            });

        });
    };


});

// LiveOrder Controller
app.controller('liveOrderCtrl', function($scope, $http, $localStorage, $interval,NgMap,geolocation) {
    console.log("Live Order Controller");

    $scope.user=$localStorage.user;

    

  

    $scope.currentGeoLoc=[];
    geolocation.getLocation().then(function(data){
         $scope.currentGeoLoc=[data.coords.latitude,data.coords.longitude];
         console.log("Current Location codr");
         console.log($scope.currentGeoLoc);
         NgMap.getMap().then(function(map) {
            console.log('map', map);
            $scope.map = map;
        });
         $scope.map.center=$scope.currentGeoLoc;
        
       });

    $scope.showPinDetail=function(event,selPoint){
        console.log("Pin Detail");
        $scope.sPoint=selPoint;
        $scope.map.showInfoWindow('myInfoWindow', this);
    }
    
    $scope.currentPage = 0;
    $scope.pageSize = 10;
    $scope.tempBookingList = [];
    $scope.q = '';
    
    $scope.getData = function () {
        $scope.tempBookingList=[];
        angular.forEach($scope.bookingList, function(value, key) {
            if(( value.pmid && value.pmid.toLowerCase().includes($scope.q.toLowerCase())) || value.reciverName.toLowerCase().includes($scope.q.toLowerCase()) ||
            value.reciverContactNo.includes($scope.q.toLowerCase()) || value.dropLocation.toLowerCase().includes($scope.q.toLowerCase()) ||
            value.deliveryType.toLowerCase().includes($scope.q.toLowerCase()) || value.dropPinCode.includes($scope.q)) {

                $scope.tempBookingList.push(value);
            }
        });  
        $scope.currentPage=0;
        $scope.numberOfPages=  Math.ceil($scope.tempBookingList.length/$scope.pageSize); 
      return $scope.tempBookingList;
     
    }
    
    // $scope.numberOfPages=function(){
    //     return Math.ceil($scope.getData().length/$scope.pageSize);                
    // }
    


    $scope.bookingList=[];
    $scope.rabbitList=[];

    $scope.getMapMarkers = function() {
      
        console.log("get All Marker");
        $http({
            method: 'GET',
            url: "/booking/rabbitsCurrentLocation",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log("response of all markers");
            console.log(response);
            $scope.rabbitLocation=response.data;
           
        }).error(function(response) {
            console.log("response erro of all markers");
            console.log(response);
           


        });
    }

   
    


    // $scope.dataOrder=[ {
    //     "statsFor": "Any Time",
    //     "notPicked": 0,
    //     "picked": 0,
    //     "delivered": 0
    //   }, {
    //     "statsFor": "10 AM-02 PM",
    //     "notPicked": 0,
    //     "picked": 0,
    //     "delivered": 0
    //   },
    //   {
    //       "statsFor": "02 PM-06 PM",
    //       "notPicked": 0,
    //       "picked": 0,
    //       "delivered": 50
    //     }, {
    //       "statsFor": "06 PM-10 PM",
    //       "notPicked": 0,
    //       "picked": 0,
    //       "delivered": 0
    //     }];


    // Fetch Live Progress bar data 
    $scope.getLiveOrdersByUser = function() {
       // showLoader(true);
        console.log("get All picup");
        $http({
            method: 'GET',
            url: baseUrl + "/booking/getLiveOrderData/" + $localStorage.user.id,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                // $.sweetModal({
                //     content: "Error",
                //     icon: $.sweetModal.ICON_ERROR
                // });
            } else {

                $scope.orderStats = response.data.floatLiveOrder;
                $scope.dataOrder = response.data.chartData;
                console.log($scope.dataOrder);
                
                var maxGraphVal=0;
                for(var k=0; k<$scope.dataOrder.length; k++){
                    var notPicVal = parseInt($scope.dataOrder[k].notPicked);
                    var picVal = parseInt($scope.dataOrder[k].picked);
                    var delVal = parseInt($scope.dataOrder[k].delivered);

                    if(k==0){
                        maxGraphVal=notPicVal+picVal+delVal;
                    }
                    if((notPicVal+picVal+delVal)>maxGraphVal){
                        maxGraphVal=notPicVal+picVal+delVal;
                    }
                }


                for (var i = 0; i < $scope.dataOrder.length; i++) {
                    var notPicVal = parseInt($scope.dataOrder[i].notPicked);
                    var picVal = parseInt($scope.dataOrder[i].picked);
                    var delVal = parseInt($scope.dataOrder[i].delivered);
                    if ((notPicVal + picVal + delVal) == 0) {
                        $scope.dataOrder[i].notPicked = 0;
                        $scope.dataOrder[i].picked = 0;
                        $scope.dataOrder[i].delivered = 0;
                    } else {
                        
                        $scope.dataOrder[i].notPicked = (notPicVal + picVal + delVal)*100/parseInt(maxGraphVal);
                        $scope.dataOrder[i].picked = (picVal + delVal) * 100 / parseInt(maxGraphVal);
                        $scope.dataOrder[i].delivered = (delVal) * 100 / parseInt(maxGraphVal);
                    }


                }
                console.log($scope.dataOrder);
                showLoader(false);
              
            }
        }).error(function(response) {
            console.log(response);
            console.log("Error occrured");
            if(response.error=="invalid_token"){
                $.sweetModal({
                    content: 'Your Session Expired ! Please login again',
                    icon: $.sweetModal.ICON_WARNING,
                });
                 $interval(function() {
                    $localStorage.$reset();
                    location.href = "login.html";
                }, 2000);
               
               
            }
            showLoader(false);
        

        });
    }

    
    
  



    //get All Booking by User
    $scope.getAllPickUpLocByUser = function(orderStatus) {
        showLoader(true);
        console.log("get All Booking");
        $http({
            method: 'GET',
            url: baseUrl + "/booking/getBookingOrder/" + $localStorage.user.id + "/status/" + orderStatus,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            console.log($localStorage.token);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.bookingList = response.data;
                $scope.tempBookingList=response.data;
                $scope.currentPage=0;
                $scope.numberOfPages=  Math.ceil($scope.tempBookingList.length/$scope.pageSize); 
               
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });

        });
    };
    
    $scope.getLiveOrdersByUser();
    $scope.getMapMarkers();
    $scope.getAllPickUpLocByUser("all");

    $interval(function() {
        $scope.getLiveOrdersByUser();
        $scope.getMapMarkers();
        $scope.getAllPickUpLocByUser("all");
    }, 10000);


});



// Delivery Statics Controller
app.controller('deliveryStatsCtrl', function($scope, $http, $localStorage,$filter) {
    console.log("Delivery Statics Controller");
    
    $scope.user=$localStorage.user;

    var cDate = new Date();
    


    $scope.currentPage = 0;
    $scope.pageSize = 10;
    $scope.tempBookingList = [];
    $scope.q = '';
    
    $scope.getData = function () {
        $scope.tempBookingList=[];
        angular.forEach($scope.bookingList, function(value, key) {
            if(value.pmid.toLowerCase().includes($scope.q.toLowerCase()) || value.reciverName.toLowerCase().includes($scope.q.toLowerCase()) ||
            value.reciverContactNo.includes($scope.q.toLowerCase()) || value.dropLocation.toLowerCase().includes($scope.q.toLowerCase()) ||
            value.deliveryType.toLowerCase().includes($scope.q.toLowerCase()) || value.dropPinCode.includes($scope.q)) {

                $scope.tempBookingList.push(value);
            }
        });
        $scope.currentPage=0;
        $scope.numberOfPages=  Math.ceil($scope.tempBookingList.length/$scope.pageSize);   
      return $scope.tempBookingList;
     
    }
    
    // $scope.numberOfPages=function(){
    //     return Math.ceil($scope.getData().length/$scope.pageSize);                
    // }
    



    $scope.offset = 0;
    $scope.timerCurrent = 0;
    $scope.uploadCurrent = 0;
    $scope.stroke = 10;
    $scope.radius = 70;
    $scope.isSemi = false;
    $scope.rounded = true;
    $scope.responsive = false;
    $scope.clockwise = true;
    $scope.currentColor = '#45ccce';
    $scope.bgColor = '#eaeaea';
    $scope.duration = 800;
    $scope.currentAnimation = 'easeOutCubic';
    $scope.animationDelay = 0;

    $scope.getStyle = function() {
        var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

        return {
            'top': $scope.isSemi ? 'auto' : '57%',
            'bottom': $scope.isSemi ? '5%' : 'auto',
            'left': '50%',
            'transform': transform,
            '-moz-transform': transform,
            '-webkit-transform': transform,
            'font-size': $scope.radius / 2 + 'px',
            'position': 'absolute',
            'height': '23%',
            'background-color': '#fff'
        };
    };

    $scope.mnths = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JULY", "AUG", "SEPT", "OCT", "NOV", "DEC"];
    $scope.barRange = ["100%", "75%", "50%", "25%", "0%"];
    // for (i = 0; i < 4; i++) {
    //     $scope.csChart.push({
    //         label:$scope.mnths[i],
    //         accepted:Math.floor(Math.random()*100),
    //         delivery:Math.floor(Math.random()*100),
    //         otif:Math.floor(Math.random()*100)
    //     });
    //  }
    console.log($scope.csChart);
    $scope.ctrl = {};


    $scope.prg = {
        receivedOrders: 0,
        acceptedOrders: 0,
        deliveredOrders: 0,
        onTimeOrders: 0
    }

    $scope.csChart=[];
    $scope.csChart=[{"delivery":"0","otif":"0","accepted":"0","label":"JAN"},
    {"delivery":"0","otif":"0","accepted":"0","label":"FEB"},
    {"delivery":"0","otif":"0","accepted":"0","label":"MAR"},
    {"delivery":"0","otif":"0","accepted":"0","label":"APR"},
    {"delivery":"0","otif":"0","accepted":"0","label":"MAY"},
    {"delivery":"0","otif":"0","accepted":"0","label":"JUN"},
    {"delivery":"0","otif":"0","accepted":"0","label":"JULY"},
    {"delivery":"0","otif":"0","accepted":"0","label":"AUG"},
    {"delivery":"0","otif":"0","accepted":"0","label":"SEPT"},
    {"delivery":"0","otif":"0","accepted":"0","label":"OCT"},
    {"delivery":"0","otif":"0","accepted":"0","label":"NOV"},
    {"delivery":"0","otif":"0","accepted":"0","label":"DEC"}];


    var arrmonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    // Fetch Live Progress bar data 
    $scope.getProgressBarDataByUser = function(crDate) {
        showLoader(true);
        console.log("get Prgress bar data");
        var crmnth=crDate.split('-')[0];
        crDate=parseInt(arrmonths.indexOf(crmnth)+1)+"-"+crDate.split('-')[1];
        console.log("Cr Date for :"+crDate);
        $http({
            method: 'GET',
            url: baseUrl + "/booking/getDeliverStaticsProgressBar/" + $localStorage.user.id + "/currdate/" + crDate,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                // $.sweetModal({
                //     content: "Error",
                //     icon: $.sweetModal.ICON_ERROR
                // });
            } else {

                $scope.prg = response.data;
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            // $.sweetModal({
            //     content: "Error",
            //     icon: $.sweetModal.ICON_ERROR
            // });

        });
    }

    $scope.barChartDataByUser = function(crDate) {
        showLoader(true);
        console.log("get Chart bar data");
        $http({
            method: 'GET',
            url: baseUrl + "/booking/getDeliverStaticsSingleChart/" + $localStorage.user.id + "/currdate/" + crDate,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                // $.sweetModal({
                //     content: "Error",
                //     icon: $.sweetModal.ICON_ERROR
                // });
            } else {

                $scope.csChart = response.data;
                console.log(JSON.stringify($scope.csChart));
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            // $.sweetModal({
            //     content: "Error",
            //     icon: $.sweetModal.ICON_ERROR
            // });

        });
    }
    
    $scope.currDate = (arrmonths[cDate.getMonth()]) + "-" + (cDate.getFullYear());
    console.log($scope.currDate);
    $scope.getProgressBarDataByUser($scope.currDate);
    $scope.currYear = cDate.getFullYear();
    $scope.barChartDataByUser("2017");

    $scope.dataOrder = [{
            "statsFor": "JAN",
            "accept": 5,
            "delivery": 7,
            "otif": 3
        }, {
            "statsFor": "FEB",
            "accept": 5,
            "delivery": 7,
            "otif": 3
        },
        {
            "statsFor": "MAR",
            "accept": 5,
            "delivery": 7,
            "otif": 3
        }, {
            "statsFor": "APR",
            "accept": 5,
            "delivery": 7,
            "otif": 3
        }
    ];


    //get All Booking by User
    $scope.getAllPickUpLocByUser = function() {
        showLoader(true);
        console.log("get All Booking");
        $http({
            method: 'GET',
            url: baseUrl + "/booking/getBookingByCustomer/" + $localStorage.user.id,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                // $scope.dataOrder=[];
                var anyTimeData = { "statsFor": "Any Time", "notPicked": 0, "picked": 0, "delivered": 0 };
                var slot1Data = { "statsFor": "10 AM-02 PM", "notPicked": 0, "picked": 0, "delivered": 0 };
                var slot2Data = { "statsFor": "02 PM-06 PM", "notPicked": 0, "picked": 0, "delivered": 0 };
                var slot3Data = { "statsFor": "06 PM-10 PM", "notPicked": 0, "picked": 0, "delivered": 0 };
                $scope.bookingList = response.data;
                for (var i = 0; i < $scope.bookingList.length; i++) {
                    if ($scope.bookingList[i].deliveryTime == 2 || $scope.bookingList[i].deliveryTime == 1) {
                        if ($scope.bookingList[i].orderStatus == "Placed")
                            anyTimeData.picked++;
                        else if ($scope.bookingList[i].orderStatus == "Delivered")
                            anyTimeData.delivered++;
                        else if ($scope.bookingList[i].orderStatus == "Schedule")
                            anyTimeData.notPicked++;
                    } else if ($scope.bookingList[i].deliveryTime == 3) {
                        if ($scope.bookingList[i].orderStatus == "Placed")
                            slot1Data.picked++;
                        else if ($scope.bookingList[i].orderStatus == "Delivered")
                            slot1Data.delivered++;
                        else if ($scope.bookingList[i].orderStatus == "Schedule")
                            slot1Data.notPicked++;
                    } else if ($scope.bookingList[i].deliveryTime == 4) {
                        if ($scope.bookingList[i].orderStatus == "Placed")
                            slot2Data.picked++;
                        else if ($scope.bookingList[i].orderStatus == "Delivered")
                            slot2Data.delivered++;
                        else if ($scope.bookingList[i].orderStatus == "Schedule")
                            slot2Data.notPicked++;
                    } else if ($scope.bookingList[i].deliveryTime == 5) {
                        if ($scope.bookingList[i].orderStatus == "Placed")
                            slot3Data.picked++;
                        else if ($scope.bookingList[i].orderStatus == "Delivered")
                            slot3Data.delivered++;
                        else if ($scope.bookingList[i].orderStatus == "Schedule")
                            slot3Data.notPicked++;
                    }
                }
                // $scope.dataOrder=[anyTimeData,slot1Data,slot2Data,slot3Data];
                // console.log($scope.dataOrder);
                // chart.dataProvider=$scope.dataOrder;
                // chart.validateData();

                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });

        });
    };
    //  $scope.getAllPickUpLocByUser();

   

    $scope.getPastOrders = function(sDate,eDate) {
        showLoader(true);
        console.log("From Date : "+sDate);
        console.log("To Date : "+eDate);

        $http({
            method: 'GET',
            url: baseUrl + "/booking/getPastBooking/"+$localStorage.user.id+"/from/"+sDate+"/to/"+eDate,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                // $.sweetModal({
                //     content: "Error",
                //     icon: $.sweetModal.ICON_ERROR
                // });
            } else {
                $scope.bookingList=response.data;
                 $scope.tempBookingList=response.data;
                 $scope.currentPage=0;
                 $scope.numberOfPages=  Math.ceil($scope.tempBookingList.length/$scope.pageSize); 
                
                showLoader(false);

            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            // $.sweetModal({
            //     content: "Error",
            //     icon: $.sweetModal.ICON_ERROR
            // });

        });
    }

    var fromDate=parseInt(cDate.getDate()-1) +"-"+ parseInt(cDate.getMonth()+1) + "-" + cDate.getFullYear();
    var toDate=parseInt(cDate.getDate()-1) +"-"+ parseInt(cDate.getMonth()+1) + "-" + cDate.getFullYear();
    $scope.getPastOrders(fromDate,toDate);


    //show search Data
    $scope.showSearchData=function(){
        $scope.tempBookingList=[];
        angular.forEach($scope.bookingList, function(value, key) {
            if(value.pmid.toLowerCase().includes($scope.q.toLowerCase()) || value.reciverName.toLowerCase().includes($scope.q.toLowerCase()) ||
            value.reciverContactNo.includes($scope.q.toLowerCase()) || value.dropLocation.toLowerCase().includes($scope.q.toLowerCase()) ||
            value.deliveryType.toLowerCase().includes($scope.q.toLowerCase()) || value.dropPinCode.includes($scope.q)) {

                $scope.tempBookingList.push(value);
            }
        });   
    }



});





// Add PickUp Location Controller
app.controller('pickUpCtrl', function($scope, $http, $localStorage) {

    $scope.savePickUpLocation = function(pickLoc) {
        showLoader(true);
        pickLoc.user = $localStorage.user;
        $http({
            method: 'POST',
            url: baseUrl + "/pickUp/addLocation",
            data: pickLoc,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            //console.log(response);
            showLoader(false);
            if (response.status == "Error") {
                // $scope.errmsg = "This user already exist";
                $.sweetModal({
                    content: "Error!" + response.message,
                    icon: $.sweetModal.ICON_ERROR
                });

            } else {

                //$scope.smsg = response.message;
                $scope.pickLoc = null;
                $scope.getAllPickUpLocByUser();
                if ($localStorage.initialApp == true) {
                    location.href = "addPackage.html";
                }
                $.sweetModal({
                    content: "Pick up Locations updated",
                    icon: $.sweetModal.ICON_SUCCESS
                });

            }
        }).error(function(response) {
            //console.log(response);
            // $scope.errmsg = response.message;
            showLoader(false);
            $.sweetModal({
                content: "Error!" + response.message,
                icon: $.sweetModal.ICON_ERROR
            });

        });
    };

    //get All Pickup Location by User
    $scope.getAllPickUpLocByUser = function() {
        showLoader(true);
        console.log("get All picup");
        $http({
            method: 'GET',
            url: baseUrl + "/pickUp/getLocationByUser/" + $localStorage.user.id,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.pickUpLocationList = response.data;
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });

        });
    };
    $scope.getAllPickUpLocByUser();


    // edit location
    $scope.editPickUpLocation = function(pickLoc) {

        $scope.pickLoc = pickLoc;
    }

    // delete location
    $scope.deletePickUpLocation = function(pickLoc) {
        console.log("delete location");
        showLoader(true);
        $http({
            method: 'GET',
            url: baseUrl + "/pickUp/deleteLocation/" + pickLoc.locationId,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            showLoader(false);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.getAllPickUpLocByUser();

                $.sweetModal({
                    content: "Success",
                    icon: $.sweetModal.ICON_SUCCESS
                });
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error !" + response.message,
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };

});



app.controller('registerCtrl', function($scope, $http) {

    $scope.smsg = $scope.errmsg = null;
    $scope.register = function() {
        if ($scope.password != $scope.cpwd) {
            $scope.errmsg = "Password does not match.";
            setTimeout(function() {
                $scope.errmsg = null;
            }, 3000);
            return;
        }
        if (isAgree == false) {
            $scope.errmsg = "Please accept terms and condition.";
            setTimeout(function() {
                $scope.errmsg = null;
            }, 3000);
            return;
        }
        var data = {
            "name": $scope.name,
            "login": $scope.username,
            "password": $scope.password,
            "roles": [{
                "id": 1
            }]
        }
        showLoader(true);
        $http({
            method: 'POST',
            url: baseUrl + "/signup",
            data: data,
            headers: {
                "Content-Type": "application/json",
            }
        }).success(function(response) {
            //console.log(response);
            if (response.status == "Error") {
                $scope.errmsg = "This user already exist";
                setTimeout(function() {
                    $scope.errmsg = null;
                }, 3000);
            } else {
                //  $scope.smsg = response.message;
                $.sweetModal({
                    content: "we are verifying you and will be in touch soon.",
                    icon: $.sweetModal.ICON_SUCCESS
                });

            }
            showLoader(false);
        }).error(function(response) {
            //console.log(response);
            $scope.errmsg = response.message;
            setTimeout(function() {
                $scope.errmsg = null;
            }, 3000);
            showLoader(false);
        });
    };
});

// Sub User Controller
app.controller('subUserCtrl', function($scope, $http, $localStorage) {
    console.log("running sub user ");
    $scope.role = $localStorage.user.roles[0].name;
    console.log($scope.role);
    $scope.saveSubUser = function(user) {
        showLoader(true);
        user.superUser = $localStorage.user;
        $http({
            method: 'POST',
            url: baseUrl + "/sub/addSubUser",
            data: user,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            showLoader(false);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.user = null;
                $scope.getAllUser();
                // if($localStorage.initialApp==true){
                //     location.href = "index2.html";
                //     $localStorage.initialApp=false;
                // }
                $.sweetModal({
                    content: "User List updated",
                    icon: $.sweetModal.ICON_SUCCESS
                });
            }
        }).error(function(response) {
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };

    //get All SubUser by User
    $scope.getAllUser = function() {
        console.log("get All User");
        showLoader(true);
        $http({
            method: 'GET',
            url: baseUrl + "/sub/getAllSubUserByUserId/" + $localStorage.user.id,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {

            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.subUserList = response.data;
                showLoader(false);
                /* $.sweetModal({
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };
    $scope.getAllUser();

    // edit Sub User
    $scope.editSubUser = function(subUser) {

        $scope.user = subUser;
    }

    // delete Sub User
    $scope.deleteSubUser = function(subUser) {
        showLoader(true);
        console.log("delete location");
        $http({
            method: 'GET',
            url: baseUrl + "/sub/deleteSubUser/" + subUser.subUserId,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.getAllUser();
                showLoader(false);
                $.sweetModal({
                    content: "Success",
                    icon: $.sweetModal.ICON_SUCCESS
                });
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error !" + response.message,
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };

});




// Single Booking Controller
app.controller('bookingCtrl', function($scope, $http, $localStorage) {
    console.log("running booking ");

    var showError=function(msg){
        $.sweetModal({
            content: msg,
            icon: $.sweetModal.ICON_ERROR
        });
    }

    $scope.generateLzWebHookToken = function(){
        showLoader(true);
        $http({
            method: 'GET',
            url: baseUrl + "/settings/generate-webhook-token",
            headers: {
                "Content-Type": "application/json"
            }
        }).success(function(response) {
            showLoader(false);
            $scope.getSettings();
        });
    }

    $scope.getSettings = function(){
        showLoader(true);
        $http({
            method: 'GET',
            url: baseUrl + "/settings/list",
            headers: {
                "Content-Type": "application/json"
            }
        }).success(function(response) {
            showLoader(false);
            $scope.settingList = response.data;
        });
    }

    $scope.deleteSetting = function(id){
        if(!confirm("Are you sure?")){
            return false;
        }
        showLoader(true);
        $http({
            method: 'GET',
            url: baseUrl + "/settings/delete-api/"+id,
            headers: {
                "Content-Type": "application/json"
            }
        }).success(function(response) {
            showLoader(false);
            if(response){
                $scope.getSettings();
            }
            else{
               $.sweetModal({
                    content: "Error! Something went wrong.",
                    icon: $.sweetModal.ICON_ERROR
                }); 
            }
        });
    }

    $scope.saveAPI = function(setting){
        
        //if(setting.matrixapikey)

        showLoader(true);
        $http({
            method: 'POST',
            url: baseUrl + "/settings/add-api",
            data: setting,
            headers: {
                "Content-Type": "application/json",
            }
        }).success(function(response) {
            showLoader(false);
            $scope.getSettings();
            $scope.setting = {};
        }).error(function(response) {
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });;

    }

    $scope.setAApi = function(setapi){
        console.log(setapi);
        $scope.setting = setapi;

        $("html, body").animate({scrollTop: 0}, 1000);
    }


    $scope.getAppointmentSlots = function(){
        showLoader(true);
        $http({
            method: 'GET',
            url: baseUrl + "/booking/get-time-slots/",
            headers: {
                "Content-Type": "application/json"
            }
        }).success(function(response) {
            showLoader(false);
            $scope.slotList = response;
        });
    }

    $scope.deleteAppointmentSlot = function(id){
        if(!confirm("Are you sure?")){
            return false;
        }
        showLoader(true);
        $http({
            method: 'GET',
            url: baseUrl + "/booking/delete-time-slots/"+id,
            headers: {
                "Content-Type": "application/json"
            }
        }).success(function(response) {
            showLoader(false);
            if(response){
                $scope.getAppointmentSlots();
            }
            else{
               $.sweetModal({
                    content: "Error! Something went wrong.",
                    icon: $.sweetModal.ICON_ERROR
                }); 
            }
        });
    }

    $scope.saveAppointmentSlot = function(){
        var appointmentDateValue = angular.element(document.querySelector("#dateValue")).val();
       
       if(appointmentDateValue == ""){

            $.sweetModal({
                content: "Enter a valid datetime",
                icon: $.sweetModal.ICON_ERROR
            });
            return false;
       }

        var appointmentDateArr = appointmentDateValue.split(/(?<=^\S+)\s/);
        var appointmentDate = appointmentDateArr[0];
        var appointmentSlot = appointmentDateArr[1];
        var data = {"date" : appointmentDate, "slots" : appointmentSlot};
   

        showLoader(true);
        console.log(data);
        $http({
            method: 'POST',
            url: baseUrl + "/booking/time-slot",
            data: data,
            headers: {
                "Content-Type": "application/json",
            }
        }).success(function(response) {
            showLoader(false);
            $scope.getAppointmentSlots();
        });

    }
    

    $scope.saveBook = function(book, reqBooking) {
       
        console.log("save booking");

        if(book==undefined){
            showError("Please Fill all Details");
            return;
        }

        if(book.pickUpLocation==undefined){
            showError("Please Select Pickup location");
            return;
        }
        var oldPicup = JSON.parse(book.pickUpLocation);
        var pickup = {};
        pickup.locationId = oldPicup.locationId;
        pickup.pickPinCode = oldPicup.pickPinCode;
        book.pickUpLocation = pickup;


        if(book.dimensions==undefined){
            showError("Please Select Package Type");
            return;
        }
        var dimObj = JSON.parse(book.dimensions);

        book.dimensions = dimObj.dimensions;
        book.weightIndex = dimObj.weightIndex;
        book.packageName = dimObj.packageName;

        book.pickupType = "SCHEDULE";
        book.pickupDateTime = $('#dateValue').val() + ":00";
        book.deliveryDate = $('#dateValue').val() + ":00";

        book.customer = $localStorage.user;
        //book.exectDimension=$scope.exectDimension.lenght+"X"+$scope.exectDimension.width+"X"+$scope.exectDimension.height;
        reqBooking.noOfOrder = "1";
        reqBooking.orderDate = $('#dateValue').val() + ":00";
        reqBooking.loginUser = $localStorage.user;
        data = {};
        if (book.deliveryType == "IMMEDIATE")
            book.deliveryTime = 1;
        else if (book.deliveryType == "ANY TIME") {
            book.deliveryTime = 2;
        }
        data.reqBooking = reqBooking;
        data.book = book;


        if (book.reciverName == null || book.reciverName == "") {
            showError("Reciver Name should be blanked");
            return;
        } else if (book.reciverContactNo == null || book.reciverContactNo == "") {
            showError("Reciver Contact Number should be blanked");
            return;
        } else if (book.dropLocation == null || book.dropLocation == "") {
            showError("Drop location should be blanked");
            return;
        } else if (book.dropLevel == null || book.dropLevel == "") {
            showError("Drop level should be blanked");
            return;
        } else if (book.unities == null || book.unities == "") {
            showError("Drop Unit should be blanked");
            return;
        } else if (book.dropPinCode == null || book.dropPinCode == "") {
            showError("Postal Code should be blanked");
            return;
        } else if (book.pickUpLocation == null || book.pickUpLocation == "") {
            showError("Please Select Pickup location");
            return;
        } else if (book.dimensions == null || book.dimensions == "") {
            showError("Please Select Package Type");
            return;
        } else if (book.deliveryTime == null || book.deliveryTime == "") {
            showError("Please Select Delivery Type");
            return;
        }

        showLoader(true);
        console.log(data);
        $http({
            method: 'POST',
            url: baseUrl + "/multiBooking/saveSingleBooking",
            data: data,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            showLoader(false);
            console.log(response);
            if (response.status == "Error") {

                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.book = null;

                $.sweetModal({
                    content: "Success",
                    icon: $.sweetModal.ICON_SUCCESS
                });
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };

    //get All Pickup Location by User
    $scope.getAllPickUpLocByUser = function() {
        showLoader(true);
        console.log("get All picup");
        $http({
            method: 'GET',
            url: baseUrl + "/pickUp/getLocationByUser/" + $localStorage.user.id,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.pickUpLocationList = response.data;
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };
    $scope.getAllPickUpLocByUser();

    //get All User
    $scope.getAllUser = function() {
        showLoader(true);
        console.log("get All User");
        $http({
            method: 'GET',
            url: baseUrl + "/sub/getAllSubUserByUserId/" + $localStorage.user.id,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.userList = response.data;
                showLoader(false);
                /* $.sweetModal({
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };
    $scope.getAllUser();

    //get All Dimions by User
    $scope.getAllPackageTypeByUser = function() {
        showLoader(true);
        console.log("get All");
        $http({
            method: 'GET',
            url: baseUrl + "/package/getPackageByUser/" + $localStorage.user.id,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {

            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.dimensionTypeList = response.data;
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };
    $scope.getAllPackageTypeByUser();
    $scope.slotOpts = [];
    $scope.initSlots = function() {
        $scope.slotOpts = [];
        var selTime = $('#dateValue').val().split(' ')[1]
        var totalMin = parseInt(selTime.split(':')[0]) * 60 + parseInt(selTime.split(':')[1]);
        console.log("Total Minute:==>" + totalMin);

        var s1Slot = { "sValue": 3, "type": "10 AM - 02 PM" };
        var s2Slot = { "sValue": 4, "type": "02 PM - 06 PM" };
        var s3Slot = { "sValue": 5, "type": "06 PM - 10 PM" };

        if (totalMin > 1080) {

        } else if (totalMin > 840) {

            $scope.slotOpts.push(s3Slot);

        } else if (totalMin > 600) {

            $scope.slotOpts.push(s2Slot);
            $scope.slotOpts.push(s3Slot);
        } else {

            $scope.slotOpts.push(s1Slot);
            $scope.slotOpts.push(s2Slot);
            $scope.slotOpts.push(s3Slot);
        }
    }
});

// Multiple Booking Controller
app.controller('multiBookingCtrl', function($scope, $http, $localStorage) {
    console.log("running multi booking ");
    $scope.picupDateTime = new Date();
    $scope.tempVahicleList = [""];
    $scope.vehicles = [];
    $scope.parseData = function(pickUpLoc) {
        var pick = JSON.parse(pickUpLoc);
        return pick;
        //  pick.pickUpLocation;
    }


    // initialise vechicle list
    $scope.orderVehicleList = [{}];
    // $scope.bookingList= [{"bookingId":"1020uI5P","pickUpLocation":null,"reciverName":null,"reciverContactNo":null,"dropLocation":null,"dropLevel":null,"unities":null,"dropPinCode":null,"weight":null,"exectWeight":null,"dimensions":null,"exectDimension":null,"deliveryDate":null,"deliveryType":null,"deliveryTime":null,"weightIndex":0,"customer":{"id":1,"name":"Neeraj Thapliyal","login":"neerajitdeveloper@gmail.com","password":"1234","active":false,"token":"214lOcI6sKS4SVnd5V10YCQmYJtRxnkIVsBXzfqlwgn9ycnWlf","companyName":"Ftechiz Solution","companyAddress":"Dobhalwala, Dehradun","website":"www.ftechiz.com","theyDo":"Software Development","needDeliver":"products","pickUpLocation":"Dobhalwal, Dehradun","pickLevel":"2","unities":"5","pickPinCode":"248001","roles":[{"id":2,"name":"ROLE_ADMIN","authority":"ROLE_ADMIN"}]},"request":{"requestId":"402880ed5f76f87f015f775eb9590003","orderDate":"2017-11-23","noOfOrder":2,"subUser":{"subUserId":1,"name":null,"email":null,"superUser":null},"loginUser":{"id":1,"name":"Neeraj Thapliyal","login":"neerajitdeveloper@gmail.com","password":"1234","active":false,"token":"214lOcI6sKS4SVnd5V10YCQmYJtRxnkIVsBXzfqlwgn9ycnWlf","companyName":"Ftechiz Solution","companyAddress":"Dobhalwala, Dehradun","website":"www.ftechiz.com","theyDo":"Software Development","needDeliver":"products","pickUpLocation":"Dobhalwal, Dehradun","pickLevel":"2","unities":"5","pickPinCode":"248001","roles":[{"id":2,"name":"ROLE_ADMIN","authority":"ROLE_ADMIN"}]}}},{"bookingId":"1020wPHK","pickUpLocation":null,"reciverName":null,"reciverContactNo":null,"dropLocation":null,"dropLevel":null,"unities":null,"dropPinCode":null,"weight":null,"exectWeight":null,"dimensions":null,"exectDimension":null,"deliveryDate":null,"deliveryType":null,"deliveryTime":null,"weightIndex":0,"customer":{"id":1,"name":"Neeraj Thapliyal","login":"neerajitdeveloper@gmail.com","password":"1234","active":false,"token":"214lOcI6sKS4SVnd5V10YCQmYJtRxnkIVsBXzfqlwgn9ycnWlf","companyName":"Ftechiz Solution","companyAddress":"Dobhalwala, Dehradun","website":"www.ftechiz.com","theyDo":"Software Development","needDeliver":"products","pickUpLocation":"Dobhalwal, Dehradun","pickLevel":"2","unities":"5","pickPinCode":"248001","roles":[{"id":2,"name":"ROLE_ADMIN","authority":"ROLE_ADMIN"}]},"request":{"requestId":"402880ed5f76f87f015f775eb9590003","orderDate":"2017-11-23","noOfOrder":2,"subUser":{"subUserId":1,"name":null,"email":null,"superUser":null},"loginUser":{"id":1,"name":"Neeraj Thapliyal","login":"neerajitdeveloper@gmail.com","password":"1234","active":false,"token":"214lOcI6sKS4SVnd5V10YCQmYJtRxnkIVsBXzfqlwgn9ycnWlf","companyName":"Ftechiz Solution","companyAddress":"Dobhalwala, Dehradun","website":"www.ftechiz.com","theyDo":"Software Development","needDeliver":"products","pickUpLocation":"Dobhalwal, Dehradun","pickLevel":"2","unities":"5","pickPinCode":"248001","roles":[{"id":2,"name":"ROLE_ADMIN","authority":"ROLE_ADMIN"}]}}}];

    $scope.addVehicle = function(reqBooking) {
        $scope.orderVehicleList.push("");
        console.log($scope.vehicles);
    }

    $scope.deleteVehicle = function(reqBooking) {
        $scope.orderVehicleList.splice(-1, 1);
        $scope.vehicles.splice(-1, 1);
    }


    $scope.saveRequest = function(reqBooking) {
        showLoader(true);
        console.log("save multi booking");
        console.log(reqBooking.subUser);
        reqBooking.orderDate = $('#dateValue').val();
        reqBooking.loginUser = $localStorage.user;
        $http({
            method: 'POST',
            url: baseUrl + "/multiBooking/saveBookingReq",
            data: reqBooking,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.reqBooking = response.data;
                console.log(JSON.stringify($scope.reqBooking));
                $scope.bookingList = [];
                for (var i = 0; i < $scope.reqBooking.noOfOrder; i++) {
                    $scope.bookingList[i] = {};
                }
                showLoader(false);
                // $.sweetModal({
                //     content: "Success",
                //     icon: $.sweetModal.ICON_SUCCESS
                // });
            }
        }).error(function(response) {
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };

    // add row 
    $scope.addRow = function() {
        showLoader(true);
        console.log("save multi booking");
        //console.log(reqBooking.subUser);
        var rowRequest = {};
        rowRequest = $scope.reqBooking;
        rowRequest.orderDate = $('#dateValue').val();
        rowRequest.noOfOrder = 1 + rowRequest.noOfOrder;
        rowRequest.loginUser = $localStorage.user;
        $http({
            method: 'POST',
            url: baseUrl + "/multiBooking/saveBookingReq",
            data: rowRequest,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.bookingList.push({});
                showLoader(false);
                // $.sweetModal({
                //     content: "Success",
                //     icon: $.sweetModal.ICON_SUCCESS
                // });
            }
        }).error(function(response) {
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };


    // delete row 
    $scope.deleteRow = function() {
        //showLoader(true);
        console.log("delete booking");
        //console.log(reqBooking.subUser);
        $scope.bookingList.splice(-1, 1);

        // $http({
        //     method: 'GET',
        //     url: baseUrl + "/booking/deleteBooking/"+$scope.bookingList[$scope.bookingList.length-1].bookingId,
        //     headers: {
        //         "Content-Type": "application/json",
        //         "Authorization": "Bearer " + $localStorage.token
        //     }
        // }).success(function(response) {
        //     console.log(response);
        //     if (response.status == "Error") {
        //         showLoader(false);
        //         $.sweetModal({
        //             content: "Error",
        //             icon: $.sweetModal.ICON_ERROR
        //         });
        //     } else {
        //         $scope.bookingList.splice(-1,1);
        //         showLoader(false);
        //         // $.sweetModal({
        //         //     content: "Success",
        //         //     icon: $.sweetModal.ICON_SUCCESS
        //         // });
        //     }
        // }).error(function(response) {
        //     console.log(response);
        //     $.sweetModal({
        //         content: "Error",
        //         icon: $.sweetModal.ICON_ERROR
        //     });
        // });
    };



    // save mulitple bookings
    $scope.saveBookings = function() {
        showLoader(true);
        console.log("save multi booking");
        // var request={
        //     requestId : bookingList[0].request.requestId
        // };
        // console.log(request);
        console.log($scope.bookingList);
        var bookingListTemp = JSON.parse(JSON.stringify($scope.bookingList));
        var checkAnyTimeCount = 0;
        for (var i = 0; i < bookingListTemp.length; i++) {
            var validValue = 0;
            if (bookingListTemp[i].reciverName == null || bookingListTemp[i].reciverName == "") {
                validValue = 1;
            } else if (bookingListTemp[i].reciverContactNo == null || bookingListTemp[i].reciverContactNo == "") {
                validValue = 1;
            } else if (bookingListTemp[i].dropLocation == null || bookingListTemp[i].dropLocation == "") {
                validValue = 1;
            } else if (bookingListTemp[i].dropLevel == null || bookingListTemp[i].dropLevel == "") {
                validValue = 1;
            } else if (bookingListTemp[i].unities == null || bookingListTemp[i].unities == "") {
                validValue = 1;
            } else if (bookingListTemp[i].dropPinCode == null || bookingListTemp[i].dropPinCode == "") {
                validValue = 1;
            } else if (bookingListTemp[i].pickUpLocation == null || bookingListTemp[i].pickUpLocation == "") {
                validValue = 1;
            } else if (bookingListTemp[i].dimensions == null || bookingListTemp[i].dimensions == "") {
                validValue = 1;
            } else if (bookingListTemp[i].deliveryTime == null || bookingListTemp[i].deliveryTime == "") {
                validValue = 1;
            }

            if (validValue == 1) {
                $.sweetModal({
                    content: "Please complete all the required data",
                    icon: $.sweetModal.ICON_ERROR
                });
                showLoader(false);
                return;
            }


            var oldPicup = JSON.parse(bookingListTemp[i].pickUpLocation);
            console.log(oldPicup);
            console.log("new pickup");
            // var pickup={};
            // pickup.locationId=oldPicup.locationId;
            // pickup.pickPinCode=oldPicup.pickPinCode;
            console.log("new pickup");
            // console.log(pickup);
            bookingListTemp[i].pickUpLocation = oldPicup;
            var dimObj = JSON.parse(bookingListTemp[i].dimensions);
            console.log(dimObj);
            bookingListTemp[i].dimensions = dimObj.dimensions;
            bookingListTemp[i].weightIndex = dimObj.weightIndex;
            bookingListTemp[i].packageName = dimObj.packageName;
            if (bookingListTemp[i].weightIndex == null) {
                if (bookingListTemp[i].dimensions.indexOf("30") >= 0) {
                    bookingListTemp[i].weightIndex = 3;
                } else if (bookingListTemp[i].dimensions.indexOf("20") >= 0) {
                    bookingListTemp[i].weightIndex = 2;
                } else {
                    bookingListTemp[i].weightIndex = 1;
                }
            }



            if (bookingListTemp[i].deliveryTime == 1)
                bookingListTemp[i].deliveryType = "IMMEDIATE";
            else if (bookingListTemp[i].deliveryTime == 2) {
                bookingListTemp[i].deliveryType = "ANY TIME";
                checkAnyTimeCount++;
            } else {
                bookingListTemp[i].deliveryType = "SLOT";
            }



            bookingListTemp[i].request = $scope.reqBooking;
            bookingListTemp[i].customer = $localStorage.user;
            bookingListTemp[i].pickupType = "SCHEDULE";
            bookingListTemp[i].pickupDateTime = $('#dateValue').val() + ":00";
            bookingListTemp[i].deliveryDate = $('#dateValue').val() + ":00";

            // if($scope.pickupDemo.pickupType="IMMEDIATE"){
            // 	var crdate=new Date();
            // 	var pcDate=crdate.getDate()+"-"+(crdate.getMonth()+1)+"-"+crdate.getFullYear()+" "+crdate.getHours()+":"+crdate.getMinutes()+":"+crdate.getSeconds();
            // 	bookingListTemp[i].pickupDateTime=pcDate;
            // 	bookingListTemp[i].deliveryDate=pcDate;
            // }


        }


        console.log(bookingListTemp);



        $http({
            method: 'POST',
            url: baseUrl + "/booking/saveMulitBookings",
            data: bookingListTemp,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            showLoader(false);
            console.log(response);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.bookingListFull = response.data;
                console.log($scope.bookingListFull);
                for (var i = 0; i < $scope.bookingList.length; i++) {
                    $scope.bookingList[i].bookingId = response.data[i].bookingId;
                    $scope.bookingList[i].id = response.data[i].id;

                }
                $scope.bookingReviews = $scope.bookingListFull;
                $('#myModal').modal('toggle');
                // $.sweetModal({
                //     content: "Routes:<br>"+response.data,
                //     icon: $.sweetModal.ICON_SUCCESS
                // });
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    }







    //show Routes
    $scope.showRoutes = function(bookingList) {
        console.log("Show Routes Running");
        console.log($scope.vehicles);
        // var tempVehicle=$scope.vehicles;
        // for(var i=0; i<tempVehicle.length; i++){
        //     var vehicleListT=JSON.parse(tempVehicle[i]);
        //     tempVehicle[i]=vehicleListT;
        // }


        var bookingsAndVeh = {
            "vehicles": $scope.vehicleList,
            "bookings": $scope.bookingListFull
        };



        $http({
            method: 'POST',
            url: baseUrl + "/booking/showRoutes",
            data: bookingsAndVeh,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            showLoader(false);
            console.log(response);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.bookingList = null;
                $scope.reqBooking = null;
                //    $scope.orderVehicleList=[{}];
                //  $scope.vehicles=null;
                $.sweetModal({
                    content: "Routes:<br>" + response.data,
                    icon: $.sweetModal.ICON_SUCCESS
                });
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });

    }

    //get All User
    $scope.getAllUser = function() {
        showLoader(true);
        console.log("get All User");
        $http({
            method: 'GET',
            url: baseUrl + "/sub/getAllSubUserByUserId/" + $localStorage.user.id,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.userList = response.data;
                showLoader(false);
                /* $.sweetModal({
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };
    $scope.getAllUser();

    //get All Dimions by User
    $scope.getAllPackageTypeByUser = function() {
        showLoader(true);
        console.log("get All");
        $http({
            method: 'GET',
            url: baseUrl + "/package/getPackageByUser/" + $localStorage.user.id,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {

            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.dimensionTypeList = response.data;
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };
    $scope.getAllPackageTypeByUser();


    //get All Pickup Location by User
    $scope.getAllPickUpLocByUser = function() {
        showLoader(true);
        console.log("get All picup");
        $http({
            method: 'GET',
            url: baseUrl + "/pickUp/getLocationByUser/" + $localStorage.user.id,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.pickUpLocationList = response.data;
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };
    $scope.getAllPickUpLocByUser();

    //get All Vehicle List
    $scope.getAllVehicles = function() {

        var selTime = $('#dateValue').val().split(' ')[1]
        var totalMin = parseInt(selTime.split(':')[0]) * 60 + parseInt(selTime.split(':')[1]);
        console.log("Total Minute:==>" + totalMin);
        $scope.slotOpts = [];
        var immediateSlot = { "sValue": 1, "type": "IMMEDIATE" };
        var anyTimeSlot = { "sValue": 2, "type": "ANY TIME" };
        var s1Slot = { "sValue": 3, "type": "10 AM - 02 PM" };
        var s2Slot = { "sValue": 4, "type": "02 PM - 06 PM" };
        var s3Slot = { "sValue": 5, "type": "06 PM - 10 PM" };

        if (totalMin > 1080) {
            $scope.slotOpts.push(immediateSlot);
            $scope.slotOpts.push(anyTimeSlot);
        } else if (totalMin > 840) {
            $scope.slotOpts.push(immediateSlot);
            $scope.slotOpts.push(anyTimeSlot);
            $scope.slotOpts.push(s3Slot);

        } else if (totalMin > 600) {
            $scope.slotOpts.push(immediateSlot);
            $scope.slotOpts.push(anyTimeSlot);
            $scope.slotOpts.push(s2Slot);
            $scope.slotOpts.push(s3Slot);
        } else {
            $scope.slotOpts.push(immediateSlot);
            $scope.slotOpts.push(anyTimeSlot);
            $scope.slotOpts.push(s1Slot);
            $scope.slotOpts.push(s2Slot);
            $scope.slotOpts.push(s3Slot);
        }

        var crLoc = window.location.href;
        console.log("Date : " + $('#dateValue').val());
        //var orderDate=$('#dateValue').html();
        //console.log(crLoc);
        var vehicleUrl = "/vehicle/getAvailVechicleByDate/" + $('#dateValue').val().split(' ')[0];
        showLoader(true);
        console.log("get All");
        $http({
            method: 'GET',
            url: baseUrl + vehicleUrl,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {

            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.vehicleList = response.data;
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };
    //$scope.getAllVehicles();
  

});



//package type controller
app.controller('packageTypeCtrl', function($scope, $http, $localStorage) {
    $scope.user = $localStorage.user;

    console.log("running packageType ");
    $scope.savePackageType = function(packageType) {
        showLoader(true);
        var crLoc = window.location.href;
        console.log("save booking");
        if (!crLoc.includes("updatePackage"))
            packageType.customer = $localStorage.user;
        $http({
            method: 'POST',
            url: baseUrl + "/package/savePackageType",
            data: packageType,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.packageType = null;
                $scope.getAllPackageType();
                if ($localStorage.initialApp == true) {
                    //  location.href = "addUser.html";
                }

                $.sweetModal({
                    content: "Success",
                    icon: $.sweetModal.ICON_SUCCESS
                });
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };

    //get All Package Type
    $scope.getAllPackageType = function() {
        $scope.userId = $localStorage.user.roles[0].id
        var crLoc = window.location.href;

        //console.log(crLoc);
        var packageTypeUrl = "/package/getPackageByUser/" + $localStorage.user.id;
        if ($scope.userId == 2 && crLoc.includes("updatePackage"))
            packageTypeUrl = "/package/getAllPackageType";
        showLoader(true);
        console.log("get All");
        $http({
            method: 'GET',
            url: baseUrl + packageTypeUrl,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {

            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.packageList = response.data;
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };
    $scope.getAllPackageType();

    // edit package type
    $scope.editPackageType = function(packageType) {

        $scope.packageType = packageType;
    }

    // delete package type
    $scope.deletePackageType = function(packageType) {
        showLoader(true);
        console.log("delete package");
        $http({
            method: 'GET',
            url: baseUrl + "/package/deletePackageType/" + packageType.packageTypeId,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {

            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.getAllPackageType();


                $.sweetModal({
                    content: "Success",
                    icon: $.sweetModal.ICON_SUCCESS
                });
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };

});



//vehicle controller
app.controller('vehicleCtrl', function($scope, $http, $localStorage) {


    console.log("running vehicleCtrl ");
    $scope.saveVehicle = function(vehicle) {
        showLoader(true);
        console.log("save vehicle");
        vehicle.owner = $localStorage.user;
        $http({
            method: 'POST',
            url: baseUrl + "/vehicle/addVehicle",
            data: vehicle,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.vehicle = null;
                $scope.getAllVehicles();
                if ($localStorage.initialApp == true) {
                    location.href = "addUser.html";
                }

                $.sweetModal({
                    content: "Success",
                    icon: $.sweetModal.ICON_SUCCESS
                });
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };

    //get All Vehicle List
    $scope.getAllVehicles = function() {
        $scope.userId = $localStorage.user.roles[0].id
        var crLoc = window.location.href;

        //console.log(crLoc);
        var vehicleUrl = "/vehicle/getAllVehicle";
        showLoader(true);
        console.log("get All");
        $http({
            method: 'GET',
            url: baseUrl + vehicleUrl,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {

            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.vehicleList = response.data;
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };
    $scope.getAllVehicles();


    // delete package type
    $scope.deleteVehicle = function(vehicle) {
        showLoader(true);
        console.log("delete vehicle");
        $http({
            method: 'GET',
            url: baseUrl + "/vehicle/deleteVehicle/" + vehicle.vehicleId,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {

            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.getAllVehicles();


                $.sweetModal({
                    content: "Success",
                    icon: $.sweetModal.ICON_SUCCESS
                });
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };

});


//Available vehicle controller
app.controller('availVehicleCtrl', function($scope, $http, $localStorage) {


    console.log("running Available vehicle controller ");
    $scope.saveVehicle = function(vehicle) {
        showLoader(true);
        console.log("save vehicle");
        vehicle.owner = $localStorage.user;
        $http({
            method: 'POST',
            url: baseUrl + "/vehicle/addVehicle",
            data: vehicle,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.vehicle = null;
                $scope.getAllVehicles();
                if ($localStorage.initialApp == true) {
                    location.href = "addUser.html";
                }

                $.sweetModal({
                    content: "Success",
                    icon: $.sweetModal.ICON_SUCCESS
                });
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };

    $scope.saveAvailVehicle = function(vehicles) {
        showLoader(true);
        console.log("save Avail vehicle");
        var availVechicleList = [];
        for (var i = 0; i < vehicles.length; i++) {

            if (vehicles[i].vehicleAvailable == true)
                availVechicleList.push({ "vehicle": vehicles[i], "orderDate": $scope.availVehicle.orderDate });
        }

        $http({
            method: 'POST',
            url: baseUrl + "/vehicle/addMultiAvailVehicle",
            data: availVechicleList,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                // $scope.vehicle=null;
                //  $scope.getAllVehicles();


                $.sweetModal({
                    content: "Success",
                    icon: $.sweetModal.ICON_SUCCESS
                });
                showLoader(false);
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };

    //get All Vehicle List
    $scope.getAllVehicles = function() {
        $scope.user = $localStorage.user;
        var crLoc = window.location.href;

        //console.log(crLoc);
        var vehicleUrl = "/vehicle/getAllVehicle";
        showLoader(true);
        console.log("get All");
        $http({
            method: 'GET',
            url: baseUrl + vehicleUrl,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {

            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.vehicleList = response.data;
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };
    $scope.getAllVehicles();

    $scope.checkAvailVehicle = function() {
        $http({
            method: 'GET',
            url: baseUrl + "/vehicle/getAllVechicleByDate/" + $scope.availVehicle.orderDate,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {

            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                var availList = response.data;
                for (var j = 0; j < $scope.vehicleList.length; j++) {
                    $scope.vehicleList[j].vehicleAvailable = false;
                    $scope.vehicleList[j].isUsed = false;
                }
                for (var i = 0; i < availList.length; i++) {
                    for (var j = 0; j < $scope.vehicleList.length; j++) {
                        if (availList[i].vehicle.vehicleId == $scope.vehicleList[j].vehicleId) {
                            $scope.vehicleList[j].vehicleAvailable = true;
                            $scope.vehicleList[j].isUsed = availList[i].used;
                        }
                    }
                }

                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    }


    // delete vehicle
    $scope.deleteVehicle = function(vehicle) {
        showLoader(true);
        console.log("delete vehicle");
        $http({
            method: 'GET',
            url: baseUrl + "/vehicle/deleteVehicle/" + vehicle.vehicleId,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {

            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.getAllVehicles();


                $.sweetModal({
                    content: "Success",
                    icon: $.sweetModal.ICON_SUCCESS
                });
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };

});

app.directive('fileModel', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function() {
                scope.$apply(function() {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);


//Pending User controller
app.controller('pendingUsersCtrl', function($scope, $http, $localStorage) {

    $scope.roles = [
            {"id":1, "name":"ROLE_USER"},
            {"id":2, "name":"ROLE_ADMIN"},
            {"id":3, "name":"ROLE_GUEST"},
            {"id":4, "name":"ROLE_TRANSPORTER"}
        ];
    $scope.userList = [];
    $scope.getInActiveUser = function(st,page) {

        $http({
            method: 'GET',
            url: baseUrl + "/userlist/"+st+"/"+page,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {

                $.sweetModal({
                    content: "Error!" + response.message,
                    icon: $.sweetModal.ICON_ERROR
                });

            } else {
                //$scope.smsg = response.message;
                $scope.userList = response.data.content;
                $scope.totalElements = response.data.totalElements;
                $scope.totalPages = response.data.totalPages;
                $scope.activePage = response.data.number;
                $scope.st = st;
            }
        }).error(function(response) {
            //console.log(response);
            // $scope.errmsg = response.message;
            $.sweetModal({
                content: "Error!" + response.message,
                icon: $.sweetModal.ICON_ERROR
            });

        });
    };

    $scope.getInActiveUser(2,0);

    $scope.getAllUsersBySt = function(st,page){
        $scope.getInActiveUser(st,page);
    }

    $scope.getPickUpForUpdate = function(pick){
        $scope.loc = pick;
    }

    $scope.getAllPickUpLocByUser = function(user,uid) {
        showLoader(true);
        console.log("get All picup");
        $scope.pickUser = user;
        $scope.loc = {};
        $scope.loc.user = user;
        $http({
            method: 'GET',
            url: baseUrl + "/pickUp/getLocationByUser/" + uid,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.pickUpLocationList = response.data;
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    };

    $scope.activateUser = function(user) {
        user.active = true;
        $http({
            method: 'GET',
            url: baseUrl + "/admin/activateUser?token=" + user.token + "&" + "email=" + user.login,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            //console.log(response);
            if (response.status == "Error") {
                // $scope.errmsg = "This user already exist";
                $.sweetModal({
                    content: "Error!" + response.message,
                    icon: $.sweetModal.ICON_ERROR
                });

            } else {
                //$scope.smsg = response.message;

                $.sweetModal({
                    content: "Success! user activated",
                    icon: $.sweetModal.ICON_SUCCESS
                });
                var rmIndex = $scope.userList.indexOf(user);
                console.log("Remove Item Index :==>" + rmIndex);
                $scope.userList.splice(rmIndex, 1);

            }
        }).error(function(response) {
            //console.log(response);
            // $scope.errmsg = response.message;
            $.sweetModal({
                content: "Error!" + response.message,
                icon: $.sweetModal.ICON_ERROR
            });

        });
    };

    $scope.makeid = function(length) {
       var result           = '';
       var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
       var charactersLength = characters.length;
       for ( var i = 0; i < length; i++ ) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
       }
       $scope.singleUser.secretKey = result;
    }

    $scope.getUserForUpdate = function(user){
        $scope.singleUser = user;
    }

    $scope.editUser = function(singleUser){
        console.log(singleUser);

        $http({
            method: 'POST',
            url: baseUrl + "/login/updateUser",
            data: singleUser,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                // $scope.vehicle=null;
                //  $scope.getAllVehicles();


                $.sweetModal({
                    content: "Success",
                    icon: $.sweetModal.ICON_SUCCESS
                });
                showLoader(false);
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });



    }

   $scope.getGmapKey = function(){
        var url = baseUrl + "/api/v1/order/gmap-key";
        console.log(url);
        $http({
            method: 'GET',
            url: url,
            headers: {
                "Content-Type": "application/json",
                // "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            $scope.gmapKey = response.key;
            console.log($scope.gmapKey);
        });
    } 

   $scope.generateLatLng = function(pickup){
    $scope.gmsg = "Searching Latitude and Longitude.... Wait....";
    var address = pickup.pickUpLocation;
    if(pickup.pickUpKelurahan != ""){
        address = address + ","+pickup.pickUpKelurahan;
    }
    if(pickup.pickUpKecematan != ""){
        address = address + ","+pickup.pickUpKecematan;
    }

    var uid = pickup.user.id;
    
    fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key='+$scope.gmapKey+'&components=country:ID')
    .then(function (response) {
        return response.json();
    })
    .then(function (data) {
        console.log(data);
        address = btoa(address);

        $http({
            method: 'POST',
            url: baseUrl + "/booking/getgeoloc/"+uid+"/"+address,
            data: data,
            headers: {
                "Content-Type": "application/json"              
            }
        }).success(function(response) {
            
            $scope.gmsg = "";
            
            if(response.postalCode != ""){
                $scope.loc.pickPinCode = response.postalCode;
                $scope.loc.pickUpKecematan = response.kecematan;
                $scope.loc.pickUpKelurahan = response.kelurahan;
                $scope.loc.pickUpLatitude = response.latitude;
                $scope.loc.pickUpLongitude = response.longitude;
                $scope.loc.pickUpKota = response.kota;
                $scope.loc.pickUpProvinsi = response.provinci;

            }
            
        }).error(function(response) {
            
        });
        
    })
    .catch(function (err) {
        $scope.gmsg = "Something went wrong!";
    });
    
    
}


$scope.savePickUpLocation = function(pickLoc) {
        showLoader(true);
        $http({
            method: 'POST',
            url: baseUrl + "/pickUp/addLocation",
            data: pickLoc,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            //console.log(response);
            showLoader(false);
            if (response.status == "Error") {
                // $scope.errmsg = "This user already exist";
                $.sweetModal({
                    content: "Error!" + response.message,
                    icon: $.sweetModal.ICON_ERROR
                });

            } else {

                //$scope.smsg = response.message;
                $scope.pickLoc = null;
                console.log(response);
                $scope.getAllPickUpLocByUser(response.data.user,response.data.user.id);
                $.sweetModal({
                    content: "Pick up Locations updated",
                    icon: $.sweetModal.ICON_SUCCESS
                });

            }
        }).error(function(response) {
            //console.log(response);
            // $scope.errmsg = response.message;
            showLoader(false);
            $.sweetModal({
                content: "Error!" + response.message,
                icon: $.sweetModal.ICON_ERROR
            });

        });
    };






});

//Peromotion controller
app.controller('promoCtrl', function($scope, $http, $localStorage) {
    console.log("Promo Controller");
    $scope.sendPermotion = function() {
        console.log("send promo" + $scope.promo);

        $http({
            method: 'POST',
            url: "http://205.147.110.207:48080/rara/promo/sendPromoNotification",
            data: $scope.promo,
            headers: {
                "Content-Type": "application/json"
            }
        }).success(function(response) {
            console.log(response);

            $.sweetModal({
                content: response.message,
                icon: $.sweetModal.ICON_SUCCESS
            });
            showLoader(false);
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    }

    $scope.regenerateRoute = function() {
        console.log("cid" + $scope.routeId);
        showLoader(true);
        $http({
            method: 'GET',
            url: baseUrl + "/booking/generateETAPoint/"+$scope.routeId,
            headers: {
                "Content-Type": "application/json"
            }
        }).success(function(response) {
            console.log(response);

            $.sweetModal({
                content: response.message,
                icon: $.sweetModal.ICON_SUCCESS
            });
            showLoader(false);
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    }

});



app.controller('bulkBookingCtrl', function($scope, $http, $localStorage) {

    $scope.demoLink = baseUrl + "/download/booking.csv";
    $scope.user = $localStorage.user;

    $scope.selPackageType=[];
      //get All Dimions by User
      $scope.getAllPackageTypeByUser = function() {
        showLoader(true);
        console.log("get All");
        $http({
            method: 'GET',
            url: baseUrl + "/package/getPackageByUser/" + $localStorage.user.id,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {

            console.log(response);
            if (response.status == "Error") {
                showLoader(false);
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {

                $scope.dimensionTypeList = response.data;
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });
    }


    $scope.getIssuedOrders = function(){
        $http({
            method: 'GET',
            url: baseUrl + "/booking/issues-order/" + $localStorage.user.id+"/0",
            headers: {
                "Content-Type": "application/json",
            }
        }).success(function(response) {

            $scope.issueOrderList = response.data.content;
            
        });
    }

    
    var issuedOrders = "#issuedOrders";
    $scope.$watch(function() { return angular.element(issuedOrders).is(':visible') }, function() {
    if(angular.element(issuedOrders).is(':visible') != true){
        return false;
    }



    $http({
            method: 'GET',
            url: baseUrl + "/booking/issues-order/" + $localStorage.user.id+"/0",
            headers: {
                "Content-Type": "application/json",
            }
        }).success(function(response) {

            $scope.issueOrderList = response.data.content;
            console.log($scope.issueOrderList);
            for(var i=0;i<$scope.issueOrderList.length;i++){
                $scope.generateLatLngOnly($scope.issueOrderList[i]);
            }
            
            $scope.getIssuedOrders();

        });




    });

    $scope.updateIssuedBookings = function(issuedBookings){
        console.log(issuedBookings);
        $http({
            method: 'POST',
            url: baseUrl + "/booking/update-issued-bookings",
            data: issuedBookings,
            headers: {
                "Content-Type": "application/json"              
            }
        }).success(function(response) {

                console.log(response);
            });
    }

    $scope.getGmapKey = function(){
        var issuedBookingList = [];
        var url = baseUrl + "/api/v1/order/gmap-key";
        console.log(url);
        $http({
            method: 'GET',
            url: url,
            headers: {
                "Content-Type": "application/json",
                // "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            $scope.gmapKey = response.key;
            console.log($scope.gmapKey);
        });
    }

    $scope.generateLatLngOnly = function(booking){
    
    showLoader(true);
    var address = booking.dropLocation;
    if(booking.dropKelurahan != ""){
        address = address + ","+booking.dropKelurahan;
    }
    if(booking.dropKecematan != ""){
        address = address + ","+booking.dropKecematan;
    }

    var uid = booking.customer.id;
    
    fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key='+$scope.gmapKey+'&components=country:ID')
    .then(function (response) {
        return response.json();
    })
    .then(function (data) {
        console.log(data);
        address = btoa(address);

        $http({
            method: 'POST',
            url: baseUrl + "/booking/getgeoloc/"+uid+"/"+address,
            data: data,
            headers: {
                "Content-Type": "application/json"              
            }
        }).success(function(response) {
            
            showLoader(false);
            
            if(response.postalCode != ""){
                var bookingArr = [];
                booking.dropPinCode = response.postalCode;
                booking.dropKecematan = response.kecematan;
                booking.dropKelurahan = response.kelurahan;
                booking.dropLatitude = response.latitude;
                booking.dropLongitude = response.longitude;
                booking.dropKota = response.kota;
                booking.dropProvinsi = response.provinci;
                bookingArr.push(booking);
                console.log(bookingArr);
                $scope.updateIssuedBookings(bookingArr);


            }
            
        }).error(function(response) {
            showLoader(false);
            
        });
        
    })
    .catch(function (err) {
        console.log("Something went wrong!", err);
    });
    
    
}

    $scope.generateLatLng = function(booking,i){
    
    showLoader(true);
    var address = booking.dropLocation;
    if(booking.dropKelurahan != ""){
        address = address + ","+booking.dropKelurahan;
    }
    if(booking.dropKecematan != ""){
        address = address + ","+booking.dropKecematan;
    }

    var uid = booking.customer.id;
    
    fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key='+$scope.gmapKey+'&components=country:ID')
    .then(function (response) {
        return response.json();
    })
    .then(function (data) {
        console.log(data);
        address = btoa(address);

        $http({
            method: 'POST',
            url: baseUrl + "/booking/getgeoloc/"+uid+"/"+address,
            data: data,
            headers: {
                "Content-Type": "application/json"              
            }
        }).success(function(response) {
            
            showLoader(false);
            
            /**/

            if(response.postalCode != ""){
                $scope.issueOrderList[i].dropPinCode = response.postalCode;
                $scope.issueOrderList[i].dropKecematan = response.kecematan;
                $scope.issueOrderList[i].dropKelurahan = response.kelurahan;
                $scope.issueOrderList[i].dropLatitude = response.latitude;
                $scope.issueOrderList[i].dropLongitude = response.longitude;
                $scope.issueOrderList[i].dropKota = response.kota;
                $scope.issueOrderList[i].dropProvinsi = response.provinci;
                
                for(j=0; j< $scope.issueOrderList.length; j++){
                    if($scope.issueOrderList[j].dropLocation == $scope.issueOrderList[i].dropLocation){
                        $scope.issueOrderList[j].dropPinCode = response.postalCode;
                        $scope.issueOrderList[j].dropKecematan = response.kecematan;
                        $scope.issueOrderList[j].dropKelurahan = response.kelurahan;
                        $scope.issueOrderList[j].dropLatitude = response.latitude;
                        $scope.issueOrderList[j].dropLongitude = response.longitude;
                        $scope.issueOrderList[j].dropKota = response.kota;
                        $scope.issueOrderList[j].dropProvinsi = response.provinci;
                    }
                }
            }
            else{

                if(response.latitude != ""){
                    var latlng = response.latitude + "," + response.longitude;

                    var url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+latlng+'&key='+$scope.gmapKey;
                    
                    fetch(url)
                    .then(function (response) {
                        return response.json();
                    })

                    .then(function (data) {
                        console.log(url);
                        console.log(data);

                        address = btoa(address);

                        $http({
                            method: 'POST',
                            url: baseUrl + "/booking/getgeolocbylatlng/"+uid+"/"+address,
                            data: data,
                            headers: {
                                "Content-Type": "application/json"              
                            }
                        })
                        .success(function(response) {

                            console.log("response");
                            console.log(response);

                            if(response.postalCode != ""){
                                $scope.issueOrderList[i].dropPinCode = response.postalCode;
                                $scope.issueOrderList[i].dropKecematan = response.kecematan;
                                $scope.issueOrderList[i].dropKelurahan = response.kelurahan;
                                $scope.issueOrderList[i].dropLatitude = response.latitude;
                                $scope.issueOrderList[i].dropLongitude = response.longitude;
                                $scope.issueOrderList[i].dropKota = response.kota;
                                $scope.issueOrderList[i].dropProvinsi = response.provinci;
                                
                                for(j=0; j< $scope.issueOrderList.length; j++){
                                    if($scope.issueOrderList[j].dropLocation == $scope.issueOrderList[i].dropLocation){
                                        $scope.issueOrderList[j].dropPinCode = response.postalCode;
                                        $scope.issueOrderList[j].dropKecematan = response.kecematan;
                                        $scope.issueOrderList[j].dropKelurahan = response.kelurahan;
                                        $scope.issueOrderList[j].dropLatitude = response.latitude;
                                        $scope.issueOrderList[j].dropLongitude = response.longitude;
                                        $scope.issueOrderList[j].dropKota = response.kota;
                                        $scope.issueOrderList[j].dropProvinsi = response.provinci;
                                    }
                                }
                            }

                        })
                        .error(function(response) {
            
                        });

                    })
                    .catch(function (err) {
                        console.log("Something went wrong!", err);
                    });


            }

            }

            /**/
            
        }).error(function(response) {
            showLoader(false);
            
        });
        
    })
    .catch(function (err) {
        console.log("Something went wrong!", err);
    });
    
    
}



    $scope.getAllPackageTypeByUser();

    $scope.uploadBooking = function(data) {
        showLoader(true);
        //var dat={};
        //dat.file=btoa(data);
        var file = $scope.myFile;
        var form = new FormData();
        form.append('file', file);
        console.log(data);
        $http({
            method: 'POST',
            url: baseUrl + "/booking/uploadCsv/"+$scope.user.id,
            data: form,
            transformRequest: angular.identity,
            headers: {
                'Content-Type': undefined,
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            showLoader(false);
            console.log(response);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error! " + response.message,
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.csvData=response.data;
                // $.sweetModal({
                //     content: "Success",
                //     icon: $.sweetModal.ICON_SUCCESS
                // });
                $("#myModal").modal({backdrop: 'static', keyboard: false});
            }
        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error! " + response.message,
                icon: $.sweetModal.ICON_ERROR
            });
        });



    }


    $scope.setPackageType=function(crDimension, crIndex){
        $scope.selPackageType[crIndex]=crDimension;
        $scope.csvData[crIndex].booking.dimensions =JSON.stringify(crDimension);     
        $scope.csvData[crIndex].errormsg.dimensions=null; 
    }


    $scope.validateOther=function(fieldName,value,crIndex){

    	
    	
        if("reciverName"==fieldName){
            if(value!=null &&  value!=""){
                $scope.csvData[crIndex].errormsg.reciverName=null;
            }
            else{
                $scope.csvData[crIndex].errormsg.reciverName="Reciver name should not be blanked";
            }
            return;
            
        }
        if("reciverContactNo"==fieldName){
        
            if(value!=null &&  !isNaN(value)){
                $scope.csvData[crIndex].errormsg.reciverContactNo=null;
            }
            else{
                $scope.csvData[crIndex].errormsg.reciverContactNo="Reciver Contact Number should be numeric";
            }
            return;
        }
        if("dropLocation"==fieldName ){
            if(value!=null && value!=""){
                $scope.csvData[crIndex].errormsg.dropLocation=null;
            }
            else{
                $scope.csvData[crIndex].errormsg.dropLocation="Drop Location should not be blanked";
            }
            return;
        }
        if("dropPinCode"==fieldName){
            if(value!=null &&  value!="" && value.length==5){
                $scope.csvData[crIndex].errormsg.dropPinCode=null;
            }
            else{
                $scope.csvData[crIndex].errormsg.dropPinCode="Postal Code should contain 6 digit";
            }
            return;
        }
        if("deliveryType"==fieldName){
            if(value!=null &&  value!=""){
                $scope.csvData[crIndex].errormsg.deliveryType=null;
            }
            else{
                $scope.csvData[crIndex].errormsg.deliveryType="Invalid Shipping Method";
            }
            return;
        }
        if("dimensions"==fieldName){
          
            if(value!=null &&  value!=""){
                $scope.csvData[crIndex].errormsg.dimensions=null;
            }
            else{
                $scope.csvData[crIndex].errormsg.dimensions="Please Select Package Type";
            }
            return;
        }
        if("dimensions"==fieldName){
            
            if(value!=null &&  value!=""){
                $scope.csvData[crIndex].errormsg.dimensions=null;
            }
            else{
                $scope.csvData[crIndex].errormsg.dimensions="Please Select Package Type";
            }
            return;
        }
        
        
    }



    $scope.validateField= function(fieldName,value,crIndex){

        console.log("fieldName: "+fieldName+" Value: "+value+" curr Index: "+crIndex);

        if($scope.user.name=="Perro Company"){
            $scope.validatePerromart(fieldName,value,crIndex);
        }
        else{
            $scope.validateOther(fieldName,value,crIndex);
        }
        

    }
    
    $scope.getSaveData = function(){
    	console.log($scope.csvData);
    }

    
    $scope.savedata= function(){
        var flag=0;
        for(var i=0; i<$scope.csvData.length; i++){
            
            if($scope.csvData[i].errormsg.reciverName!=null){
                flag=1;
                break;
            }
            if($scope.csvData[i].errormsg.reciverContactNo!=null){
                 flag=1;
                break;
            }
            if($scope.csvData[i].errormsg.dropLocation!=null){
                 flag=1;
                break;
            }
            if($scope.csvData[i].errormsg.dropPinCode!=null){
                flag=1;
                break;
            }
            if($scope.csvData[i].errormsg.deliveryType!=null){
                flag=1;
                break;
            }
            if($scope.csvData[i].errormsg.paymentType!=null){
                flag=1;
                break;
            }
            if($scope.csvData[i].errormsg.amount!=null){
                flag=1;
                break;
            }
            if($scope.csvData[i].errormsg.dimensions!=null){
                flag=1;
                break;
            }
            // if($scope.csvData[i].errormsg.dropKelurahan!=null){
            //     flag=1;
            //     break;
            // }
            // if($scope.csvData[i].errormsg.dropKecematan!=null){
            //     flag=1;
            //     break;
            // }
            // if($scope.csvData[i].errormsg.dropKota!=null){
            //     flag=1;
            //     break;
            // }
            // if($scope.csvData[i].errormsg.dropProvinsi!=null){
            //     flag=1;
            //     break;
            // }
            if($scope.csvData[i].errormsg.pickUpLocation.pickUpLocation!=null){
                flag=1;
                break;
            }
            if($scope.csvData[i].errormsg.pickUpLocation.pickUpKelurahan!=null){
                flag=1;
                break;
            }
            if($scope.csvData[i].errormsg.pickUpLocation.pickUpKecematan!=null){
                flag=1;
                break;
            }
            if($scope.csvData[i].errormsg.pickUpLocation.pickUpKota!=null){
                flag=1;
                break;
            }
            if($scope.csvData[i].errormsg.pickUpLocation.pickUpProvinsi!=null){
                flag=1;
                break;
            }
            if($scope.csvData[i].errormsg.pickUpLocation.pickPinCode!=null){
                flag=1;
                break;
            }
            
           
            var dimObj = $scope.csvData[i].booking.dimensions;
            
            $scope.csvData[i].booking.dimensions = dimObj.dimensions;
            $scope.csvData[i].booking.weightIndex = dimObj.weightIndex;
            $scope.csvData[i].booking.packageName = dimObj.packageName;
            if ($scope.csvData[i].booking.weightIndex == null) {
                $scope.csvData[i].booking.weightIndex=1;
            }
            if($scope.csvData[i].booking.packageName == "Small")
                $scope.csvData[i].booking.weightIndex=1;
            if($scope.csvData[i].booking.packageName == "Medium")
                $scope.csvData[i].booking.weightIndex=2;
            if($scope.csvData[i].booking.packageName == "Large")
                $scope.csvData[i].booking.weightIndex=3;
        }

        if(flag==1){
            $.sweetModal({
                content: "Please Submit correct detail",
                icon: $.sweetModal.ICON_ERROR
            });
            return;
        }

        console.log($scope.csvData);
        showLoader(true);
        $http({
            method: 'POST',
            url: baseUrl + "/booking/saveCsvData/"+$scope.user.id,
            data: $scope.csvData,
            headers: {
                "Content-Type": "application/json"
            }
        }).success(function(response) {
            console.log(response);

            $.sweetModal({
                content: response.message,
                icon: $.sweetModal.ICON_SUCCESS
            });
            showLoader(false);
                $('#myModal').modal('toggle');
//                location.href = "liveOrders.html";

        }).error(function(response) {
            showLoader(false);
            console.log(response);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });
        });

    }


});

app.filter('range', function() {
      return function(input, total) {
        total = parseInt(total);
        for (var i=0; i<total; i++)
          input.push(i);
        return input;
      };
    });