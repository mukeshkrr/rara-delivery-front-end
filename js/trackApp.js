angular.module('ngMap', []);
var app = angular.module("ropid", ['ngStorage', 'ngMap','ngFitText']);
app.config( [
    '$compileProvider',
    function( $compileProvider )
    {   
        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome-extension|tel|sms):/);
        // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
    }
]);
baseUrl = "http://localhost:8080";
// var baseUrl = "http://business.rara.delivery";
var isAgree = false;


var showLoader = function(show) {

    if (show == true) {
        document.getElementById("loader").style.visibility = "visible";
        document.getElementById("loader").style.zIndex = "1000";
    } else {
        document.getElementById("loader").style.visibility = "hidden";
    }


}



app.controller('orderTrackingCtrl', function($scope, $http, $localStorage,$interval) {
    
    $scope.get1 = function(){
        return [
            {"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]}
       ];
    }

    $scope.getDetail=function(){

      showLoader(true);

      var trackId=window.location.href.split('?')[1].split('=')[1];

      $http({
        method: 'GET',
        url: baseUrl + "/booking/ordertracking/"+trackId,
        headers: {
            "Content-Type": "application/json"
        }
      }).success(function(response) {
          console.log(response);
          $scope.track=response;
          switch(response.orderStatus){
              case "Start Delivery":$scope.track.showStatus="Rabbit is on the way to deliver your parcel";
              break;
              case "Arrived at Dropoff":$scope.track.showStatus="Rabbit has arrived at your location";
              break;
              case "Authenticate":
              case "Delivered":
              $scope.track.showStatus="Your parcel is delivered";
              break;

          }
          
          $scope.rabbitLocation=[$scope.track];
          showLoader(false);


      }).error(function(response) {
          showLoader(false);
          console.log(response);
          $.sweetModal({
              content: "Error",
              icon: $.sweetModal.ICON_ERROR
          });
      });

    }

    $scope.getDetail();

    // $interval(function() {
    //     $scope.getDetail();
    // }, 10000);

    
});
