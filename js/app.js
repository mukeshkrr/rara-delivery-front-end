angular.module('ngMap', []);
var app = angular.module("ropid", ['ngStorage', 'ngMap', 'angular-svg-round-progressbar',
'ui.bootstrap','ui.carousel','geolocation']);
baseUrl = "http://"+window.location.host;
//baseUrl = "http://localhost:8081";
// var baseUrl = "http://205.147.110.207:48080/rara-b2c";
//baseUrl = "http://101.53.139.53:28080/rarab2c/";
var isAgree = false;

app.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
}});

app.filter('myFormat', function() {
    return function(x) {
    return x.slice(0,1);
        
    };
});

var showLoader = function(show) {

    if (show == true) {
        document.getElementById("loader").style.visibility = "visible";
        document.getElementById("loader").style.zIndex = "1000";
    } else {
        document.getElementById("loader").style.visibility = "hidden";
    }


}
