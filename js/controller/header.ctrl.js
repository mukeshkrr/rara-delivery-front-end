app.controller('headerCtrl', function($scope, $http, $localStorage, $interval) {


    $scope.user = $localStorage.user;
    if ($scope.user == null) {
        location.href = "login.html";
    }

    $scope.logout = function() {
        $localStorage.$reset();
        location.href = "login.html";

    };

    $scope.getProfile = function() {

        location.href = "company-profile.html";

    };
    //$scope.checkAdmin()
    var path = window.location.href.split("/");
    $scope.crFile = path[path.length - 1];
    console.log("Cr File : " + $scope.crFile);
    $scope.menuList = [];

    $scope.getGmapKey = function(){
        var url = baseUrl + "/api/v1/order/gmap-key";
        console.log(url);
        $http({
            method: 'GET',
            url: url,
            headers: {
                "Content-Type": "application/json",
                // "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            $scope.gmapKey = response.key;
            $scope.checkPreOrdersInstant();

        });
    }
    

    $scope.generateLatLng = function(booking){
    if(booking.zoneId > 0 || booking.latitude != 0){
        $scope.updateOrders(booking);
        return false;
    }
    var address = booking.addressLine1+booking.addressLine2;
    var uid = booking.user.id;
    fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key='+$scope.gmapKey+'&components=country:ID')
    .then(function (response) {
        return response.json();
    })
    .then(function (data) {
        console.log(data);
        address = btoa(address);

        $http({
            method: 'POST',
            url: baseUrl + "/booking/getgeoloc/"+uid+"/"+address,
            data: data,
            headers: {
                "Content-Type": "application/json"              
            }
        }).success(function(response) {
            
            
            if(response.postalCode != ""){
                booking.postalCode = response.postalCode;
                booking.kecematan = response.kecematan;
                booking.kelurahan = response.kelurahan;
                booking.latitude = response.latitude;
                booking.longitude = response.longitude;
                booking.kota = response.kota;
                booking.provinci = response.provinci;
                booking.zoneId = response.zoneId;
                
                $scope.updatePreBooking(booking);
            }
            
        }).error(function(response) {
            
        });
        
    })
    .catch(function (err) {
        console.log("Something went wrong!", err);
    });
    
    
}

$scope.updatePreBooking = function(booking){
    console.log(booking);
    if(booking.postalCode == "" || booking.latitude == "" || booking.longitude == "" || booking.addressLine1 == "" || booking.bookingId == ""){
        return false;
    }
    if(booking.postalCode == 0 || booking.latitude == 0) return false;
    
    
    $http({
        method: 'POST',
        url: baseUrl + "/booking/update-preorders",
        data: booking,
        headers: {
            "Content-Type": "application/json"              
        }
    }).success(function(response) {
        
        $scope.updateOrders(booking);
        
    }).error(function(response) {
       
    });
    
}

$scope.updateOrders = function(booking){
    var bookings = [];
   
    if(booking.zoneId == 0 || booking.postalCode == 0 ) return false;
    if(booking.pickUpLocation.pickUpLatitude == null || booking.pickUpLocation.pickUpLatitude == 0 ) return false;
    bookings.push(booking);

     console.log(bookings);
    $http({
        method: 'POST',
        url: baseUrl + "/booking/update-bookings",
        data: bookings,
        headers: {
            "Content-Type": "application/json"              
        }
    }).success(function(response) {
        
        console.log(response);
        
    }).error(function(response) {
       
       console.log(response);
    });
}

    if ($scope.user.roles[0].id == 2) {
        

        $scope.menuList.push({
            "name": "Settings",
            "url": "settings.html",
            "icon": "fa fa-dashboard",
            "active": ($scope.crFile.includes("settings.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });

        $scope.menuList.push({
            "name": "Live Orders",
            "url": "adminliveOrders.html",
            "icon": "fa fa-dashboard",
            "active": ($scope.crFile.includes("liveOrders.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });
        $scope.menuList.push({
            "name": "Delivery Statistics",
            "url": "admindashborad.html",
            "icon": "fa fa-dashboard",
            "active": ($scope.crFile.includes("dashboard.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });
        

        $scope.menuList.push({
            "name": "Add Appointment Slot",
            "url": "appointment-slots.html",
            "icon": "fa fa-dashboard",
            "active": ($scope.crFile.includes("appointment-slots.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });

        $scope.menuList.push({
            "name": "Create a Zone",
            "url": "create-zone.html",
            "icon": "fa fa-dashboard",
            "active": ($scope.crFile.includes("create-zone.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });
        
        $scope.menuList.push({
            "name": "Generated Route",
            "url": "generatedRoute.html",
            "icon": "fa fa-dashboard",
            "active": ($scope.crFile.includes("generatedRoute.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });
        $scope.menuList.push({
            "name": "Update Custom Package",
            "url": "updatePackage.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("updatePackage.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });
        $scope.menuList.push({
            "name": "Set Vehicle Availability",
            "url": "availableVehicle.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("availableVehicle.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });
        $scope.menuList.push({
            "name": "User List",
            "url": "userlist.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("userlist.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });
        $scope.menuList.push({
            "name": "Drivers",
            "url": "driver-list.html",
            "icon": "fa fa-taxi",
            "active": ($scope.crFile.includes("driver-list.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });
        $scope.menuList.push({
            "name": "Scanners",
            "url": "scanners.html",
            "icon": "fa fa-taxi",
            "active": ($scope.crFile.includes("scanners.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });
        
        $scope.menuList.push({
            "name": "Regenerate ETA Points",
            "url": "regenerateRoute.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("regenerateRoute.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });
        $scope.menuList.push({
            "name": "Check Pre Orders",
            "url": "check-orders.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("check-orders.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });

        $scope.menuList.push({
            "name": "Order status",
            "url": "orders.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("orders.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });


        $scope.checkPreOrdersInstant = function(){
            
          
                var url = baseUrl + "/api/v1/order/preorderlist";
                console.log(url);
                $http.get(url)
                   .then(function(response){
                       console.log("Preorder instant check");
                       for(var i=0;i<response.data.length;i++){
                        $scope.generateLatLng(response.data[i]);
                       }
                       
                   }).finally(function(){
                   });
            
        };

        $scope.getGmapKey();


    }
    if ($scope.user.roles[0].id == 1) {
        $scope.menuList.push({
            "name": "Live Orders",
            "url": "liveOrders.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("liveOrders.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });
        $scope.menuList.push({
            "name": "Delivery Statistics",
            "url": "dashboard.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("dashboard.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });
        $scope.menuList.push({
            "name": "Place an Order",
            "url": "advanced.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("advanced.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')

        });
        $scope.menuList.push({
            "name": "Place Multiple Orders",
            "url": "multipleBooking.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("multipleBooking.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });

        $scope.menuList.push({
            "name": "Excel Upload",
            "url": "uploadfile.html",
            "icon": "fa fa-files-o",
            "active": ($scope.crFile.includes("uploadfile.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });

         $scope.menuList.push({
            "name": "Order Status",
            "url": "orders.html",
            "icon": "glyphicon glyphicon-calendar",
            "active": ($scope.crFile.includes("orders.html") ? 'background-color:#5d5c5c' : 'backgroud-color:#333')
        });

    }
    
    if ($scope.user.roles[0].id == 2) {
    	$scope.ecnt = 0;
 
    	$scope.getFrequentErr = function(){
    		
    		$interval(function () {
    		    
    			
    			var url = baseUrl + "/error/exceptions/0";
            	console.log(url);
            	$http.get(url)
            	   .then(function(response){
            		   var errCnt = parseInt($("#errCnt").text());
            		   $scope.errList = response.data;
            		   var newErrCnt = $scope.errList.exceptions.numberOfElements;
            		   
            		   console.log("errCnt:"+errCnt+" newErrCnt:"+newErrCnt);
            		   if(errCnt != newErrCnt){
            			   $('#errModal').css("display","block");
            		   }
            		   if(newErrCnt == 0){
            			   $('#errModal').css("display","none");
            		   }
            		   $scope.ecnt = newErrCnt;
            		   console.log($scope.errList);
            	   }).finally(function(){

            	   });
    			
    			
    		  }, 2000);
    		
    	};
    	
    	//$scope.getFrequentErr();
    	

    	
    	$scope.closeErrModal = function(){
    		$('#errModal').css("display","none");
    	}
    	
    	$(function () {
    		$("#errCheck").click(function(){
        		
        		$('#errModal').css("display","block");
        		
        	});
    	});
    	
    	
    	$scope.viewErrDetails = function(msg){
    		
    		$("#detailsViewBody").text(msg);
    		$("#detailsView").css("display","block");
    		
    	}
    	
    	$scope.closeDetailViewModal = function(){
    		
    		$("#detailsViewBody").text("");
    		$("#detailsView").css("display","none");
    		
    	}
    	
    	$scope.errResolved = function(id){
    		
    		if(!confirm("Are you sure?")){
    			return false;
    		}
    		var url = baseUrl + "/error/resolved/"+id;
        	console.log(url);
        	$http.get(url)
        	   .then(function(response){
        		   
        		   var errCnt = parseInt($("#errCnt").text());
        		   $("#errCnt").text(errCnt - 1);
        		   var newErrCnt = $scope.errList.exceptions.numberOfElements - 1;
        		   
        		   $scope.ecnt = newErrCnt;
        		   $(".err_"+id).remove();
        	  
        	   }).finally(function(){

        	   });
    	}
    	
    	
    	
    }
    
    
    
    
   



});
