// LiveOrder Controller
app.controller('genRouteCtrl', function($scope, $http, $localStorage, $interval) {
    console.log("Route Controller");
    
    

    $scope.user=$localStorage.user;
    // NgMap.getMap().then(function(map) {
    //     console.log('map', map);
    //     $scope.map = map;
    // });

    // var map = L.map('map');

   // var map = L.map('map').setView([51.505, -0.09], 13);

    // L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}{r}.png', {
    //     attribution: '© OpenStreetMap contributors'
    // }).addTo(map);

// L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
//     attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
//     maxZoom: 18,
//     id: 'mapbox.streets',
//     accessToken: 'pk.eyJ1IjoibXVrZXNoa3JyIiwiYSI6ImNqd2htdnF4NDA0ZXk0YW9hMnRzdXU5bXkifQ.TFCLAgh-OZndnQu9F2Qm8g'
// }).addTo(map);




//     var orgIcon = L.icon({
//         iconUrl: '../../images/pin.png',
//         iconSize: [38, 95],
//         iconAnchor: [22, 94],
//         popupAnchor: [-3, -76],
//         shadowSize: [68, 95],
//         shadowAnchor: [22, 94]
//     });

    // var orgIcon = new LeafIcon({iconUrl: '../../images/pin.png'}),
    // destIcon = new LeafIcon({iconUrl: '../../images/drop.png'});

   

    $scope.showPinDetail=function(event,selPoint){
        console.log("Pin Detail");
        $scope.sPoint=selPoint;
        $scope.map.showInfoWindow('myInfoWindow', this);
    }

    $scope.tabs = [];
      var lastwayp=[];
      var routing= [];


      $scope.changeTab=function(seltab,pickUpLat,pickUpLng,pickUpLocation){
        $scope.selTab=seltab;
        $scope.bookingList = $scope.selTab.bookings;
        $scope.tempBookingList=$scope.selTab.bookings;
        $scope.currentPage=0;
        $scope.numberOfPages=  Math.ceil($scope.tempBookingList.length/$scope.pageSize); 
        getRouteOnMap($scope.tempBookingList,pickUpLat,pickUpLng,pickUpLocation);

      }

    $scope.currentPage = 0;
    $scope.pageSize = 10;
    $scope.tempBookingList = [];
    $scope.q = '';
    
    $scope.getData = function () {
        $scope.tempBookingList=[];
        angular.forEach($scope.bookingList, function(value, key) {
            if(( value.pmid && value.pmid.toLowerCase().includes($scope.q.toLowerCase())) || value.reciverName.toLowerCase().includes($scope.q.toLowerCase()) ||
            value.reciverContactNo.includes($scope.q.toLowerCase()) || value.dropLocation.toLowerCase().includes($scope.q.toLowerCase()) ||
            value.deliveryType.toLowerCase().includes($scope.q.toLowerCase()) || value.dropPinCode.includes($scope.q)) {

                $scope.tempBookingList.push(value);
            }
        });  
        $scope.currentPage=0;
        $scope.numberOfPages=  Math.ceil($scope.tempBookingList.length/$scope.pageSize); 
      return $scope.tempBookingList;
     
    }
    
    


    $scope.bookingList=[];
    $scope.rabbitList=[];


    //get All Booking by User
    $scope.getRouteData = function() {
        
        if($scope.companyName==undefined || $scope.currDate==undefined || $scope.currDate==""){
            return;
        }
        showLoader(true);
        console.log("get All Booking");
        console.log($scope.companyName.id);
        console.log($scope.currDate);


        $http({
            method: 'GET',
            url: baseUrl + "/booking/getBookingRouteAdmin/" + $scope.companyName.id + "/" + $scope.currDate,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.compData=response;
                $scope.tabs=[];
                $scope.bookingList = []
                $scope.tempBookingList=[];
                for(var i=0;i<$scope.compData.grpCon.length;i++){
                    var wayPnt=[];
                    


                    var myRoute = $scope.compData.grpCon[i].corder[0].route.routePath;
                    var myRoute2ndIndex = myRoute.indexOf("Route for",1);
                    var trace = myRoute.substring(0, myRoute2ndIndex);
                    trace = trace.replace(/ to /gi,'<br /><span class="glyphicon glyphicon-download"></span> ');
                    
                    var pickUpLat = parseFloat($scope.compData.grpCon[i].corder[0].pickUpLocation.pickUpLatitude);
                    var pickUpLng = parseFloat($scope.compData.grpCon[i].corder[0].pickUpLocation.pickUpLongitude);
                    var pickUpLocation = $scope.compData.grpCon[i].corder[0].pickUpLocation.pickUpLocation;
                    var rabbitId = ($scope.compData.grpCon[i].rabbit != null)?$scope.compData.grpCon[i].rabbit.firstname+' '+$scope.compData.grpCon[i].rabbit.lastname+ ' ('+ $scope.compData.grpCon[i].rabbit.mobileno+')':'';
                    $scope.tabs.push(
                        {   title:'Consignment '+(i+1), 
                            totalCOrder:$scope.compData.grpCon[i].corder.length, 
                            waypoints: wayPnt,
                            bookings:$scope.compData.grpCon[i].corder,
                            totalDistance : $scope.compData.grpCon[i].totalDistance,
                            zone:$scope.compData.grpCon[i].zone,
                            route:trace,
                            pickUpLat:pickUpLat,
                            pickUpLng:pickUpLng,
                            pickUpLocation:pickUpLocation,
                            rabbitId:rabbitId,
                            consignmentId: $scope.compData.grpCon[i].corder[0].route.routeId

                        });

                        
                }
                if($scope.tabs.length>0){
                    $scope.changeTab($scope.tabs[0],pickUpLat,pickUpLng,pickUpLocation);
                }
                

               
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response.error);
            showLoader(false);
            var errMsg = "Error";
            if(response.error == "invalid_token"){
                errMsg = "Invalid Token. Login again to access this page.";
            }
            $.sweetModal({
                content: errMsg,
                icon: $.sweetModal.ICON_ERROR
            });

            $localStorage.$reset();
            location.href = "login.html";

        });
    };
   


    $scope.getCompanyList=function(){

        showLoader(true);
        console.log("get Company List");
        $http({
            method: 'GET',
            url: baseUrl + "/api/companyList",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.companyList = response;
                
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });

        });        
    }


    $scope.getCompanyList();

    $scope.companyName={id:0, companyName:"All"};
    var cDate=new Date();
    $scope.currDate="0"+parseInt(cDate.getDate()-1) +"-0"+ parseInt(cDate.getMonth()+1) + "-" + cDate.getFullYear();

    $scope.getRouteData();

});

