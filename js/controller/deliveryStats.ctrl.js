// Delivery Statics Controller
app.controller('deliveryStatsCtrl', function($scope, $http, $localStorage,$filter) {
    console.log("Delivery Statics Controller");
    
    $scope.user=$localStorage.user;
    
    var cDate = new Date();
    
    $scope.deliveryInit = function(){
        $scope.download = false;
        $scope.downloadLink = "";
    }

    $scope.currentPage = 0;
    $scope.pageSize = 10;
    $scope.tempBookingList = [];
    $scope.q = '';
    
    $scope.getData = function () {
        $scope.tempBookingList=[];
        angular.forEach($scope.bookingList, function(value, key) {
            if(value.pmid.toLowerCase().includes($scope.q.toLowerCase()) || value.reciverName.toLowerCase().includes($scope.q.toLowerCase()) ||
            value.reciverContactNo.includes($scope.q.toLowerCase()) || value.dropLocation.toLowerCase().includes($scope.q.toLowerCase()) ||
            value.deliveryType.toLowerCase().includes($scope.q.toLowerCase()) || value.dropPinCode.includes($scope.q)) {

                $scope.tempBookingList.push(value);
            }
        });
        $scope.currentPage=0;
        $scope.numberOfPages=  Math.ceil($scope.tempBookingList.length/$scope.pageSize);   
      return $scope.tempBookingList;
     
    }
    
    // $scope.numberOfPages=function(){
    //     return Math.ceil($scope.getData().length/$scope.pageSize);                
    // }
    



    $scope.offset = 0;
    $scope.timerCurrent = 0;
    $scope.uploadCurrent = 0;
    $scope.stroke = 10;
    $scope.radius = 70;
    $scope.isSemi = false;
    $scope.rounded = true;
    $scope.responsive = false;
    $scope.clockwise = true;
    $scope.currentColor = '#45ccce';
    $scope.bgColor = '#eaeaea';
    $scope.duration = 800;
    $scope.currentAnimation = 'easeOutCubic';
    $scope.animationDelay = 0;

    $scope.getStyle = function() {
        var transform = ($scope.isSemi ? '' : 'translateY(-50%) ') + 'translateX(-50%)';

        return {
            'top': $scope.isSemi ? 'auto' : '57%',
            'bottom': $scope.isSemi ? '5%' : 'auto',
            'left': '50%',
            'transform': transform,
            '-moz-transform': transform,
            '-webkit-transform': transform,
            'font-size': $scope.radius / 2 + 'px',
            'position': 'absolute',
            'height': '23%',
            'background-color': '#fff'
        };
    };

    $scope.mnths = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JULY", "AUG", "SEPT", "OCT", "NOV", "DEC"];
    $scope.barRange = ["100%", "75%", "50%", "25%", "0%"];
    // for (i = 0; i < 4; i++) {
    //     $scope.csChart.push({
    //         label:$scope.mnths[i],
    //         accepted:Math.floor(Math.random()*100),
    //         delivery:Math.floor(Math.random()*100),
    //         otif:Math.floor(Math.random()*100)
    //     });
    //  }
    console.log($scope.csChart);
    $scope.ctrl = {};


    $scope.prg = {
        receivedOrders: 0,
        acceptedOrders: 0,
        deliveredOrders: 0,
        onTimeOrders: 0
    }

    $scope.csChart=[];
    $scope.csChart=[{"delivery":"0","otif":"0","accepted":"0","label":"JAN"},
    {"delivery":"0","otif":"0","accepted":"0","label":"FEB"},
    {"delivery":"0","otif":"0","accepted":"0","label":"MAR"},
    {"delivery":"0","otif":"0","accepted":"0","label":"APR"},
    {"delivery":"0","otif":"0","accepted":"0","label":"MAY"},
    {"delivery":"75","otif":"23","accepted":"25","label":"JUN"},
    {"delivery":"0","otif":"0","accepted":"0","label":"JULY"},
    {"delivery":"0","otif":"0","accepted":"0","label":"AUG"},
    {"delivery":"0","otif":"0","accepted":"0","label":"SEPT"},
    {"delivery":"0","otif":"0","accepted":"0","label":"OCT"},
    {"delivery":"0","otif":"0","accepted":"0","label":"NOV"},
    {"delivery":"0","otif":"0","accepted":"0","label":"DEC"}];


    var arrmonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    // Fetch Live Progress bar data 
    $scope.getProgressBarDataByUser = function(crDate) {
        showLoader(true);
        console.log("get Prgress bar data");
        var crmnth=crDate.split('-')[0];
        crDate=parseInt(arrmonths.indexOf(crmnth)+1)+"-"+crDate.split('-')[1];
        console.log("Cr Date for :"+crDate);
        $http({
            method: 'GET',
            url: baseUrl + "/booking/getDeliverStaticsProgressBar/" + $scope.selCId + "/currdate/" + crDate,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                // $.sweetModal({
                //     content: "Error",
                //     icon: $.sweetModal.ICON_ERROR
                // });
            } else {

                $scope.prg = response.data;
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            // $.sweetModal({
            //     content: "Error",
            //     icon: $.sweetModal.ICON_ERROR
            // });

        });
    }

    $scope.barChartDataByUser = function(crDate) {
        showLoader(true);
        console.log("get Chart bar data");
        $http({
            method: 'GET',
            url: baseUrl + "/booking/getDeliverStaticsSingleChart/" + $scope.selCId + "/currdate/" + crDate,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                // $.sweetModal({
                //     content: "Error",
                //     icon: $.sweetModal.ICON_ERROR
                // });
            } else {

                $scope.csChart = response.data;
                console.log(JSON.stringify($scope.csChart));
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            // $.sweetModal({
            //     content: "Error",
            //     icon: $.sweetModal.ICON_ERROR
            // });

        });
    }
    
    

    $scope.dataOrder = [{
            "statsFor": "JAN",
            "accept": 5,
            "delivery": 7,
            "otif": 3
        }, {
            "statsFor": "FEB",
            "accept": 5,
            "delivery": 7,
            "otif": 3
        },
        {
            "statsFor": "MAR",
            "accept": 5,
            "delivery": 7,
            "otif": 3
        }, {
            "statsFor": "APR",
            "accept": 5,
            "delivery": 7,
            "otif": 3
        }
    ];


    //get All Booking by User
    $scope.getAllPickUpLocByUser = function() {
        showLoader(true);
        console.log("get All Booking");
        $http({
            method: 'GET',
            url: baseUrl + "/booking/getBookingByCustomer/" + $localStorage.user.id,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                // $scope.dataOrder=[];
                var anyTimeData = { "statsFor": "Any Time", "notPicked": 0, "picked": 0, "delivered": 0 };
                var slot1Data = { "statsFor": "10 AM-02 PM", "notPicked": 0, "picked": 0, "delivered": 0 };
                var slot2Data = { "statsFor": "02 PM-06 PM", "notPicked": 0, "picked": 0, "delivered": 0 };
                var slot3Data = { "statsFor": "06 PM-10 PM", "notPicked": 0, "picked": 0, "delivered": 0 };
                $scope.bookingList = response.data;
                for (var i = 0; i < $scope.bookingList.length; i++) {
                    if ($scope.bookingList[i].deliveryTime == 2 || $scope.bookingList[i].deliveryTime == 1) {
                        if ($scope.bookingList[i].orderStatus == "Placed")
                            anyTimeData.picked++;
                        else if ($scope.bookingList[i].orderStatus == "Delivered")
                            anyTimeData.delivered++;
                        else if ($scope.bookingList[i].orderStatus == "Schedule")
                            anyTimeData.notPicked++;
                    } else if ($scope.bookingList[i].deliveryTime == 3) {
                        if ($scope.bookingList[i].orderStatus == "Placed")
                            slot1Data.picked++;
                        else if ($scope.bookingList[i].orderStatus == "Delivered")
                            slot1Data.delivered++;
                        else if ($scope.bookingList[i].orderStatus == "Schedule")
                            slot1Data.notPicked++;
                    } else if ($scope.bookingList[i].deliveryTime == 4) {
                        if ($scope.bookingList[i].orderStatus == "Placed")
                            slot2Data.picked++;
                        else if ($scope.bookingList[i].orderStatus == "Delivered")
                            slot2Data.delivered++;
                        else if ($scope.bookingList[i].orderStatus == "Schedule")
                            slot2Data.notPicked++;
                    } else if ($scope.bookingList[i].deliveryTime == 5) {
                        if ($scope.bookingList[i].orderStatus == "Placed")
                            slot3Data.picked++;
                        else if ($scope.bookingList[i].orderStatus == "Delivered")
                            slot3Data.delivered++;
                        else if ($scope.bookingList[i].orderStatus == "Schedule")
                            slot3Data.notPicked++;
                    }
                }
                // $scope.dataOrder=[anyTimeData,slot1Data,slot2Data,slot3Data];
                // console.log($scope.dataOrder);
                // chart.dataProvider=$scope.dataOrder;
                // chart.validateData();

                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });

        });
    };
    //  $scope.getAllPickUpLocByUser();

    $scope.getDownloadLink = function(){
        showLoader(true);

        $http({
          url: baseUrl + "/booking/download-bookings/"+$scope.selCId+"/from/"+$scope.sDate+"/to/"+$scope.eDate,
          method: 'GET',
          transformResponse: [function (response) {
              // Data response is plain text at this point
              // So just return it, or do your parsing here
              $scope.downloadLink = "";
              if(response != "No"){
                $scope.download = false;
                $scope.downloadLink = response;
              }
              showLoader(false);
              return response;
          }]
        });

    }

    $scope.getPastOrders = function(sDate,eDate) {
        showLoader(true);
        console.log("From Date : "+sDate);
        console.log("To Date : "+eDate);
        console.log(baseUrl + "/booking/getPastBooking/"+$scope.selCId+"/from/"+sDate+"/to/"+eDate);
        $scope.sDate = sDate;
        $scope.eDate = eDate;

        $http({
            method: 'GET',
            url: baseUrl + "/booking/getPastBooking/"+$scope.selCId+"/from/"+sDate+"/to/"+eDate,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                // $.sweetModal({
                //     content: "Error",
                //     icon: $.sweetModal.ICON_ERROR
                // });
            } else {
                $scope.bookingList=response.data;
                 $scope.tempBookingList=response.data;
                 $scope.currentPage=0;
                 $scope.numberOfPages=  Math.ceil($scope.tempBookingList.length/$scope.pageSize); 
                 $scope.download = true;  
                 $scope.downloadLink = ""; 
                showLoader(false);

            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            // $.sweetModal({
            //     content: "Error",
            //     icon: $.sweetModal.ICON_ERROR
            // });

        });
    }

    


    //show search Data
    $scope.showSearchData=function(){
        $scope.tempBookingList=[];
        angular.forEach($scope.bookingList, function(value, key) {
            if(value.pmid.toLowerCase().includes($scope.q.toLowerCase()) || value.reciverName.toLowerCase().includes($scope.q.toLowerCase()) ||
            value.reciverContactNo.includes($scope.q.toLowerCase()) || value.dropLocation.toLowerCase().includes($scope.q.toLowerCase()) ||
            value.deliveryType.toLowerCase().includes($scope.q.toLowerCase()) || value.dropPinCode.includes($scope.q)) {

                $scope.tempBookingList.push(value);
            }
        });   
    }


    $scope.getCompanyList=function(){

        showLoader(true);
        console.log("get Company List");
        $http({
            method: 'GET',
            url: baseUrl + "/api/companyList",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.companyList = response;
                
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });

        });        
    }


    $scope.getCompanyList();


    $scope.getCompanyData=function(cmp){
        console.log(cmp);
        $scope.selCId=cmp.id;
        $scope.currDate = (arrmonths[cDate.getMonth()]) + "-" + (cDate.getFullYear());
        console.log($scope.currDate);
        $scope.getProgressBarDataByUser($scope.currDate);
        $scope.currYear = cDate.getFullYear();
        $scope.barChartDataByUser("2019");

        var fromDate=parseInt(cDate.getDate()-1) +"-"+ parseInt(cDate.getMonth()+1) + "-" + cDate.getFullYear();
        var toDate=parseInt(cDate.getDate()-1) +"-"+ parseInt(cDate.getMonth()+1) + "-" + cDate.getFullYear();
        $scope.getPastOrders(fromDate,toDate);
    }

   var initc={id:0};

    $scope.getCompanyData(initc);



});

