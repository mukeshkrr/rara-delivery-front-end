app.controller('zoneCtrl', function(Base64, $scope, $http, $localStorage, NgMap) {
	
	$scope.zoneList = [];
    $scope.zoneBookingList = [];
    $scope.pickuptime = [
        "09:00","10:00","11:00","12:00","13:00","14:00","15:00","16:00",
        "17:00","18:00","19:00","20:00","21:00"
    ];
    $scope.deliveryInit = function(){
        $scope.download = false;
        $scope.downloadLink = "";
        $scope.sDate = moment().startOf('month').format('DD-MM-YYYY');
        $scope.eDate = moment().endOf('month').format('DD-MM-YYYY');
    }
	
	//zoneService.getZoneByPin(14140);
	$scope.getOrderResponsesImages = function(id,st){
		//showLoader(true);
		
        var url = baseUrl + "/booking/bsri/"+id+"/"+st;
        console.log(url);
        $http({
            method: 'GET',
            url: url,
            headers: {
                "Content-Type": "application/json",
                // "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status != "Success") {
                $.sweetModal({
                    content: response.message,
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                response.data.filter(function(x){
                    return (x.image != null);
                });
                $scope.orderResponseImageList = response.data;
            
               // showLoader(false);
                
            }
        }).error(function(response) {
            console.log(response);
            if(response.data == null){
            	alert("No image found");
            }

        });
	} 

	$scope.getOrderResponses = function(id){
		//showLoader(true);

        var url = baseUrl + "/booking/bsr/"+id;
        console.log(url);
        $http({
            method: 'GET',
            url: url,
            headers: {
                "Content-Type": "application/json",
                // "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status != "Success") {
                $.sweetModal({
                    content: response.message,
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.orderResponseList = response.data;
               
               // showLoader(false);
                
            }
        }).error(function(response) {
            console.log(response);
           // showLoader(false);
            

        });
	}  

    $scope.getGmapKey = function(){
        var url = baseUrl + "/api/v1/order/gmap-key";
        console.log(url);
        $http({
            method: 'GET',
            url: url,
            headers: {
                "Content-Type": "application/json",
                // "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            $scope.gmapKey = response.key;
            console.log($scope.gmapKey);
        });
    }

	
	$scope.getOrderList = function(page) {

		var uid = 0;

		
		
		if(typeof $scope.userlist !== "undefined"){
			uid = $scope.userlist;
		}

		if($localStorage.user.roles[0].name == 'ROLE_USER'){
			uid = $localStorage.user.id;
		}
		
        $scope.selCId = uid;
    
        showLoader(true);
        var bid = ($scope.searchBooking == "")?"s":$scope.searchBooking;
        var url = baseUrl + "/booking/list/"+uid+"/"+$scope.sDate+"/"+$scope.eDate+"/"+page+"/"+bid;
        console.log(url);
        $http({
            method: 'GET',
            url: url,
            headers: {
                "Content-Type": "application/json",
                // "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status != "Success") {
                $.sweetModal({
                    content: response.message,
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.orderList = response.data.content;
                $scope.totalPages = response.data.totalPages;
                $scope.totalElements = response.data.totalElements;
                $scope.activePage = page;
                $scope.download = true;
                $scope.downloadLink = "";
                console.log("totalPages"+$scope.totalPages);

                showLoader(false);
                
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });

        });
    }

    $scope.selectByBusiness = function(){
        $scope.searchBooking = "s";
        $scope.getOrderList(0);
    }

    var orderlist = "#orderlist";
    $scope.$watch(function() { return angular.element(orderlist).is(':visible') }, function() {
        if(angular.element(orderlist).is(':visible') != true){
            return false;
        }
       $scope.isAdmin = true; 
       if($localStorage.user.roles[0].name == 'ROLE_USER'){
       	$scope.isAdmin = false; 
       }
       $scope.getAllUserList(); 
      // $scope.getOrderList(0); 

    });




	
	  var districtList = "#districtList";
		$scope.$watch(function() { return angular.element(postalList).is(':visible') }, function() {
		    if(angular.element(districtList).is(':visible') != true){
		    	return false;
		    }

		   
		   
		   var catUrl = baseUrl+"/zone/districtList";
		   $http.get(catUrl,{ cache: true})
			   .then(function(response){

			   	$scope.districtList = response.data;
			   	console.log(response.data);
			   }).finally(function(){

			   });

			
		});  // On loading districtList loading section
		
		var consumerZoneList = "#consumerZoneList";
		$scope.$watch(function() { return angular.element(postalList).is(':visible') }, function() {
		    if(angular.element(consumerZoneList).is(':visible') != true){
		    	return false;
		    }

		   
		   
		    var url = baseUrl + "/zone/userbyrole/1";
			console.log(url);
			$http.get(url,{ cache: true})
			   .then(function(response){

				   $scope.userListByRole = response.data;
				   
			   });

			
		});  // On loading userList loading section	
		
		
	$scope.getZonesByBusiness = function(uid){

        showLoader(true);

        var catUrl = baseUrl+"/zone/zonelist/"+uid;
       $http.get(catUrl,{ cache: true})
           .then(function(response){

            $scope.zoneList = response.data;
            console.log("Auto Data"+response.data);
           }).finally(function(){
                showLoader(false);
           });

    }

	
	var postalList = "#zoneList";
	$scope.$watch(function() { return angular.element(postalList).is(':visible') }, function() {
	    if(angular.element(postalList).is(':visible') != true){
	    	return false;
	    }

	   


		
	});  // On loading zoneList loading section
	
	
	var zoneWithBookingList = "#zoneWithBookingList";
	$scope.$watch(function() { return angular.element(zoneWithBookingList).is(':visible') }, function() {
	    if(angular.element(zoneWithBookingList).is(':visible') != true){
	    	return false;
	    }

	   
	   
	   var catUrl = baseUrl+"/zone/zone-booking-list";
	   $http.get(catUrl,{ cache: true})
		   .then(function(response){

		   	$scope.zoneBookingList = response.data;
		   	console.log(response.data);
		   }).finally(function(){

		   });

		
	});  // On loading zoneList loading section
	
	
    $scope.getBookingZoneListByBusiness = function(uid){

       var catUrl = baseUrl+"/zone/zone-booking-list/"+uid;
       console.log(catUrl);
       $http.get(catUrl)
           .then(function(response){

            $scope.zoneBookingList = response.data;
            console.log(response.data);
           }).finally(function(){

           });
    }
	
	  
	$scope.example15model = [];  $scope.example15settings = { enableSearch: true }; $scope.customFilter = '';	
	
	$scope.setSubDistrict = function(){
		$scope.subDistList = [];
		$scope.zipList = [];
		var district = $scope.selectedDistrict;
		var catUrl = baseUrl+"/zone/subdistrictList/"+district;
		   $http.get(catUrl,{ cache: true})
			   .then(function(response){

				
			   	var length = response.data.length;
			   	var responses = [];
			   	for(var i=0;i<length;i++){
			   		
			   		responses[i] = {id:response.data[i], label:response.data[i]};
			   		
			   	}
			   	
			   	$scope.example15data = responses;
			   	
			   }).finally(function(){

			   });
		
	}
	
	$scope.getZipCodesBySubDistricts = function(data){
		var idArr = [];
        $scope.chk = [];
		data.forEach(function(item, index){
			
			idArr.push(item.id);
			
		});
		
		var ids = idArr.join();
		ids = (ids == null || ids == '')?0:ids;
		var catUrl = baseUrl+"/zone/postalbysubdistricts/"+ids;
		console.log(catUrl);
		   $http.get(catUrl,{ cache: true})
			   .then(function(response){
				$scope.zipList = response.data;   
                $scope.zipListFiltered = [];
                for(var i=0;i<$scope.zipList.length;i++){

                    $scope.zipListFiltered.push($scope.zipList[i].zipcode+"_"+$scope.zipList[i].village);
                    $scope.zipCodeList = $scope.zipListFiltered;
                    for(var i=0; i < $scope.zipList.length; i++){
                        $scope.chk[i] = false;
                    }
                }
			   	
			   }).finally(function(){

			   });
		
		
		
		
	}
	
	
	$scope.subDistList = [];
	
	$scope.getSubDistrictsForZipCodes = {
		onInitDone: function(item) {console.log(item);},
		onItemDeselect: function(item) {
			$scope.subDistList.splice($scope.subDistList.indexOf(item), 1);
			$scope.getZipCodesBySubDistricts($scope.subDistList);
			
		},
		onItemSelect: function(item){
				$scope.subDistList.push(item);
				$scope.getZipCodesBySubDistricts($scope.subDistList);
			},
		onSelectAll: function(){ 
			$scope.subDistList = $scope.example15data;
			$scope.getZipCodesBySubDistricts($scope.subDistList);
		},
		onDeselectAll: function(){
				$scope.subDistList = [];
				$scope.getZipCodesBySubDistricts($scope.subDistList);
			}
	};
	
	
	
$scope.updateZone = function(zone) {
    	
    	if(zone.zoneName == null || zone.zoneName == ''){
    		alert("Zone title is invalid");
    		return false;
    	}
    	if(zone.zoneId == null || zone.zoneId == null){
    		alert("Zone ID is invalid");
    		return false;
    	}
    	
    	
    	var data = {zoneId:zone.zoneId,zoneName:zone.zoneName};
    	
    	console.log(data);

        $http({
            method: 'POST',
            url: baseUrl + "/zone/updatezone",
            data: data,
            headers: {
                "Content-Type": "application/json"              
            }
        }).success(function(response) {

            $scope.msgUpdateType =response.status;
            $scope.msgUpdate = response.message;
            
            if($scope.msgUpdateType == "success"){
                $.sweetModal({
                        content: $scope.msgUpdate,
                        icon: $.sweetModal.ICON_SUCCESS
                     });
            }
            if($scope.msgUpdateType == "error"){
                $.sweetModal({
                    content: $scope.msgUpdate,
                    icon: $.sweetModal.ICON_ERROR
                });
            }
        	  
        	
            
        }).error(function(response) {
            console.log(response);
           
        });
    };
	
	
	$scope.getZoneList = function(){
		var catUrl = baseUrl+"/zone/zonelist";
		   $http.get(catUrl)
			   .then(function(response){

			   	
			   }).finally(function(){

			   });
	}
	
	
	$scope.checkedZipCodes = [];
	$scope.zipCodeList = [];
    $scope.allChecked = false;
    $scope.chk = [];
    $scope.selectAllPostalCode = function(){
        if($scope.allChecked)
            $scope.zipCodeList = $scope.zipListFiltered;
        else
            $scope.zipCodeList = [];

        for(var i=0; i < $scope.zipList.length; i++){
            $scope.chk[i] = ($scope.zipCodeList.length > 0)?true:false;

            if($scope.chk[i])
            $scope.toggleCheckZipCode($scope.zipList[i]);


        }
    }
	
    $scope.toggleCheckZipCode = function (zip) {
        if ($scope.checkedZipCodes.indexOf(zip) === -1) {
            $scope.checkedZipCodes.push(zip);
            $scope.zipCodeList.push(zip.zipcode+"_"+zip.village);
        } else {
            $scope.checkedZipCodes.splice($scope.checkedZipCodes.indexOf(zip), 1);
            $scope.zipCodeList.splice($scope.zipCodeList.indexOf(zip.zipcode+"_"+zip.village), 1);

           
        }
        
    };
    
    $scope.viewZone = function(zoneName,ZoneId){
        $scope.zoneData = [];
    	$scope.zoneTitle = zoneName;
        $scope.das = true;
    	$scope.loadZoneData(ZoneId);
    }
    
    
    $scope.addZipCode = function() {
    	
    	if($scope.selectedZoneName == null || $scope.selectedZoneName == ''){
    		alert("Select a zone from zone list");
    		return false;
    	}
    	if($scope.selectedDistrict == null || $scope.selectedDistrict == null){
    		alert("Select a district from list");
    		return false;
    	}
    	if($scope.subDistList.length == 0){
    		alert("Select at least one sub district from list");
    		return false;
    	}
    	if($scope.zipCodeList == null || $scope.zipCodeList == '' || $scope.zipCodeList == []){
    		alert("Select postal codes from postal code list");
    		return false;
    	}
    	showLoader(true);
    	$scope.zonePostal = {zone_name:$scope.selectedZoneName,zipcodes:$scope.zipCodeList,zone_id:$scope.selectedZoneId};

        $http({
            method: 'POST',
            url: baseUrl + "/zone/savezone",
            data: $scope.zonePostal,
            headers: {
                "Content-Type": "application/json"              
            }
        }).success(function(response) {
            
	           var catUrl = baseUrl+"/zone/zonelist";
	     	   $http.get(catUrl)
	     		   .then(function(response){

	     		   	$scope.zoneList = response.data;
	     		   	console.log(catUrl+"Auto Data"+response);
	     		   	
	     		   }).finally(function(){
	
	     	   });
        	console.log(response);
        	$scope.msgZipType =response.status;
        	$scope.msgZip = response.message;
        	location.reload(); 
            
        }).error(function(response) {
            console.log(response);
           
        }).finally(function(){
                showLoader(false);
               });
    };
    
    $scope.readZone = function(zoneName,zoneId){
    	$scope.selectedZoneName = zoneName;
    	$scope.selectedZoneId = zoneId;
        var scrollBottom = $(window).scrollTop() + $(window).height();
        $("html, body").animate({scrollTop: scrollBottom}, 1000);
    }
    
    
    $scope.deleteAZone = function(zoneId,uId){
    		if(!confirm("Do you want to delete this zone?")){
    			return false;
    		}
    		
    		var url = baseUrl + "/zone/deletezone/"+zoneId;
    		console.log(url);
    		$http.get(url,{ cache: true})
			   .then(function(response){

				   var catUrl = baseUrl+"/zone/zonelist/"+uId;
		     	   $http.get(catUrl)
		     		   .then(function(response){

		     		   	$scope.zoneList = response.data;
		     		   	console.log($scope.zoneList);
		     		   	
		     		   }).finally(function(){
		
		     	   });
				   
			   }).finally(function(){

			   });
    		
    		
    		
    	
    }
    
    $scope.addAZone = function(){
    	if($scope.zoneName == '' || $scope.zoneName == null){
    		$scope.msgType = 'error';
        	$scope.msg = 'Zone name field is mandatory';
        	return false;
    	}
        if($scope.uId == '' || $scope.uId == null){
            $scope.msgType = 'error';
            $scope.msg = 'Business field is mandatory';
            return false;
        }
    	$scope.zoneData = {zoneName:$scope.zoneName, userId:$scope.uId};
        console.log($scope.zoneData);
        $http({
            method: 'POST',
            url: baseUrl + "/zone/addazone",
            data: $scope.zoneData,
            headers: {
                "Content-Type": "application/json"              
            }
        }).success(function(response) {
            
	           var catUrl = baseUrl+"/zone/zonelist/"+$scope.uId;
	     	   $http.get(catUrl)
	     		   .then(function(response){

	     		   	$scope.zoneList = response.data;
	     		   	console.log($scope.zoneList);
	     		   	
	     		   }).finally(function(){
	
	     	   });
        	console.log(response);
            
        	$scope.msgType = response.status;
        	$scope.msg = response.message;

            if($scope.msgType == "success"){
                $.sweetModal({
                        content: $scope.msg,
                        icon: $.sweetModal.ICON_SUCCESS
                     });
            }
            if($scope.msgType == "error"){
                $.sweetModal({
                    content: $scope.msg,
                    icon: $.sweetModal.ICON_ERROR
                });
            }


        	
        }).error(function(response) {
            console.log(response);
           
        });
    }
    
    
    
    $scope.loadZoneData = function(data){
    	$("#myModal").modal("show");

    	
    	$scope.getZone(data);
    	
    	
    }
    
    $scope.deleteZipZoneMapping = function(zid,zoneZipMapId){
        console.log(zid);
        console.log(zoneZipMapId);
        if(!confirm("Are you sure?")) return false;

        showLoader(true);
        var url = baseUrl + "/zone/delete-zone-zip-map/"+zid+"/"+zoneZipMapId;
        console.log(url);
        $http.get(url,{ cache: true})
           .then(function(response){

              
           }).finally(function(){
            showLoader(false);
            $scope.zoneData = [];
            $scope.das = true;
            $scope.getZone(zid);

           });





    }
    
    $scope.getZone = function(zoneId){
    	
    	var url = baseUrl + "/zone/getzone/"+zoneId;
		console.log(url);
		$http.get(url,{ cache: false})
		   .then(function(response){

			   $scope.zoneData = response.data;
			   $scope.initMap(response.data);
			   
		   }).finally(function(){
                $scope.das = false;
		   });
		
    	
    }
    
    var map,map1;
    $scope.initMap = function(d) {
      map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: {lat: -6.226996, lng: 106.819894},
        zoomControl: true,
         zoomControlOptions: {
          position: window.google.maps.ControlPosition.RIGHT_CENTER
         },
         scrollwheel: false,
         streetViewControl: false,
         mapTypeControl: false,
         mapTypeId: 'roadmap',
      });
      
      // NOTE: This uses cross-domain XHR, and may not work on older browsers.
      d.forEach(function(val,index){
    	  map.data.loadGeoJson(
          'geojsons/'+val.zipcodes.geojson_file);
      });
      
    }
    
    
    $scope.getUserListByRole = function(role){
    	
    	var url = baseUrl + "/zone/userbyrole/"+role;
		console.log(url);
		$http.get(url,{ cache: true})
		   .then(function(response){

			   $scope.userListByRole = response.data;
			   
		   }).finally(function(){

		   });
		
    	
    }
    
    
$scope.mapConsumerAndZone = function() {
    	
    	if($scope.selectedConsumer == null || $scope.selectedConsumer == ''){
    		alert("Select a consumer from consumer list");
    		return false;
    	}
    	if($scope.selectedZone == null || $scope.selectedZone == null){
    		alert("Select a zone from list");
    		return false;
    	}
    	
    	
    	var data = {zoneId:$scope.selectedZone,id:$scope.selectedConsumer};
    	
    	console.log(data);

        $http({
            method: 'POST',
            url: baseUrl + "/zone/save-consumer-zone",
            data: data,
            headers: {
                "Content-Type": "application/json"              
            }
        }).success(function(response) {
            
        	  
        	$scope.msgUserZoneType =response.status;
        	$scope.msgUserZone = response.message;
        	$scope.getZonesByConsumer();
            
        }).error(function(response) {
            console.log(response);
           
        });
    };
    
    
    $scope.getZonesByConsumer = function(){
    	
    	if($scope.selectedConsumer == '' || $scope.selectedConsumer == null){
    		alert("Select a consumer first.");
    		return false;
    	}
    	
    	var url = baseUrl + "/zone/getzonebyuser/"+$scope.selectedConsumer;
		console.log(url);
		$http.get(url)
		   .then(function(response){

			   $scope.userZoneMapList = response.data;
			   console.log(response.data);
			   
		   }).finally(function(){

		   });
		
    	
    }
    
$scope.deleteZonesByConsumer = function(id){
	
			if(!confirm("Are you sure, You want to delete?")) return false;
			
			
			var url = baseUrl + "/zone/deleteZonesByConsumer/"+id;
			console.log(url);
			$http.get(url)
			   .then(function(response){
				   
				   $scope.getZonesByConsumer();
				   
			   });
			
    }


$scope.getDriverList = function(){
	
	
	var url = baseUrl + "/zone/drivers";
	console.log(url);
	$http.get(url)
	   .then(function(response){

		   $scope.driverList = response.data;
		   
	   }).finally(function(){

	   });
		
}

var driverlist = "#driverlist";
$scope.$watch(function() { return angular.element(postalList).is(':visible') }, function() {
    if(angular.element(driverlist).is(':visible') != true){
    	return false;
    }

   
   
    $scope.getDriverList();

	
});  // On loading districtList loading section



$scope.deleteAZoneOfDriver = function(zoneId){
	
	if(!confirm("Do you want to delete this zone?")){
		return false;
	}
	
	var url = baseUrl + "/zone/deletezoneofdriver/"+zoneId;
	console.log(url);
	$http({
	    method: 'DELETE',
	    url: url
	    
	})
	.then(function(response) {
	    console.log(response.data);
	    
	    $(".zone_"+zoneId).html("");
	    
	    
	}, function(rejection) {
	    console.log(rejection.data);
	});

	
	
	

}


    
    
$scope.mapDriverZone = function(zoneId, driverId, zoneName){
	
	console.log(zoneName);

	var data = {zoneId:zoneId,driverId:driverId};
	
	console.log(data);

    $http({
        method: 'POST',
        url: baseUrl + "/zone/save-driver-zone",
        data: data,
        headers: {
            "Content-Type": "application/json"              
        }
    }).success(function(response) {
        
    	console.log(driverId);  
    	$scope.msgDriverZoneType =response.status;
    	$scope.msgDriverZone = response.message;
        
    }).error(function(response) {
        console.log(response);
       
    });
		
}


$scope.generateRoute = function(data){
	console.log(data);
	$scope.generateMsg = 'Wait! Route is generating...';
	$scope.msgUpdateType = 'success';
	
	$http({
        method: 'POST',
        url: baseUrl + "/zone/generate-route",
        data: data,
        headers: {
            "Content-Type": "application/json"              
        }
    }).success(function(response) {
    	
    	$scope.generateMsg = response.message;
    	$scope.msgUpdateType = response.status;
        
        if(response.status == "success" && response.message == "Route generated successfully") {
            $scope.generateRoute(data);
        }   
        
           location.reload(); 
        
        
    }).error(function(response) {
        console.log(response);
       
    });
	
	
	
	
}

$scope.generateRouteByBusiness = function(uId){

    

    if (typeof $scope.pt === 'undefined'){
        alert("Select a Pickup time");
        return false;
    }
    else{
        console.log($scope.pt);
    }

    if(!confirm("Are you sure?")) return false;



    $scope.generateMsg = 'Wait! Route is generating...';
    $scope.msgUpdateType = 'success';
    if(parseInt(uId) <= 0){
        alert("Select a valid Business");
        return false;
    }
    $http({
        method: 'GET',
        url: baseUrl + "/vr/algo/"+uId+"/"+$scope.pt,
        headers: {
            "Content-Type": "application/json"              
        }
    }).success(function(response) {
        //debugger;
        $scope.generateMsg = response.message;
        $scope.msgUpdateType = response.status;
        
          
        
        $scope.getBookingZoneListByBusiness(uId);

           //location.reload(); 
        
        
    }).error(function(response) {
        console.log(response);
       
    });
   
}

$scope.unDoGeneratedRoute = function(uId){

    if(!confirm("Reset routing is unrecoverable process. Do you want to reset routes? ")) return false;

    $scope.generateMsg = 'Wait! Process started...';
    $scope.msgUpdateType = 'success';
    if(parseInt(uId) <= 0){
        alert("Select a valid Business");
        return false;
    }
    $http({
        method: 'GET',
        url: baseUrl + "/vr/undo/"+uId,
        headers: {
            "Content-Type": "application/json"              
        }
    }).success(function(response) {
        //debugger;
        $scope.generateMsg = response.message;
        $scope.msgUpdateType = response.status;

        $scope.getBookingZoneListByBusiness(uId);

           //location.reload(); 
        
        
    }).error(function(response) {
        console.log(response);
       
    });
   
}



$scope.getAllUserList = function() {
    showLoader(true);
    console.log("get All User");
    $http({
        method: 'GET',
        url: baseUrl + "/api/userList",
       
    }).success(function(response) {
        console.log(response);
        $scope.userList = response;
        showLoader(false);
        
    }).error(function(response) {
        showLoader(false);
        console.log(response);
       
    });
};

$scope.getZoneListByUser = function(userId){
    var catUrl = baseUrl+"/zone/zonelist/"+userId;
       $http.get(catUrl,{ cache: true})
           .then(function(response){

            $scope.zoneList = response.data;
            console.log(response.data);
           }).finally(function(){

           });
}

var checkOrdersId = "#checkOrdersId";
$scope.$watch(function() { return angular.element(checkOrdersId).is(':visible') }, function() {
    if(angular.element(checkOrdersId).is(':visible') != true){
    	return false;
    }
    $scope.showLatLng = false;
    $scope.packageNames = ["Small","Medium","Large","Xtra Large","Double Xtra Large", "Triple Xtra Large"];
    $scope.deliveryTimes = ["Any","Slot 1","Slot 2","Slot 3"];
    
    $scope.sno = false;
    $scope.bookingId = $scope.zone = $scope.postalCode = true;
    $scope.receiverName = $scope.airwayId = false;
    $scope.receiverEmail = false;
    $scope.receiverContact = $scope.deliveryTime = false;
    $scope.orderId = false;$scope.dimensions = true;
    $scope.addressLine1 = $scope.addressLine2 = true;
    $scope.kecematan = $scope.kelurahan  = $scope.latitude = $scope.longitude = false; 
    
    $scope.getAllUserList();
    
    var catUrl = baseUrl+"/zone/zonelist";
	   $http.get(catUrl,{ cache: true})
		   .then(function(response){

		   	$scope.zoneList = response.data;
		   	console.log(response.data);
		   }).finally(function(){

		   });
    
    
    
    
    
});




$scope.getPreOrderList = function(id){
    $scope.getZoneListByUser(id);
	 showLoader(true);
	    console.log("get All User");
	    $http({
	        method: 'GET',
	        url: baseUrl + "/booking/getpreorders/"+id,
	       
	    }).success(function(response) {
	        console.log(response);
	        $scope.preOrderList = response;
	        showLoader(false);
	        
	        var noZoneList = response.filter(function (item) {
	            return item.zoneId == 0;
	        });
	        var noPostalCodeList = response.filter(function (item) {
	            return item.postalCode == null;
	        });

            var latAvailableOrdersList = response.filter(function (item) {
                return (item.latitude != 0 && item.zoneId > 0);
            });
	        console.log("No Zone");
	        console.log(noZoneList.length);
	        
	        console.log("No Postal");
	        console.log(noPostalCodeList.length);

            console.log(latAvailableOrdersList);

            if(latAvailableOrdersList.length > 0){
                $scope.updateOrders(latAvailableOrdersList);

                for(var j=0; j < latAvailableOrdersList.length; j++){
                   $scope.removePreBooking(latAvailableOrdersList[j],0);
                }
            }
	        
	        
	    }).error(function(response) {
	        showLoader(false);
	        console.log(response);
	       
	    });
}



$scope.updatePreBooking = function(booking){
	console.log(booking);
	if(booking.postalCode == "" || booking.latitude == "" || booking.longitude == "" || booking.addressLine1 == "" || booking.bookingId == ""){
		alert("All fields are mandatory");
		return false;
	}
	
	
	$http({
        method: 'POST',
        url: baseUrl + "/booking/update-preorders",
        data: booking,
        headers: {
            "Content-Type": "application/json"              
        }
    }).success(function(response) {
        
    	console.log(response);
        
    }).error(function(response) {
        console.log(response);
       
    });
	
	
	
	
}

$scope.generateLatLng = function(booking,i){
	
	showLoader(true);
	var address = booking.addressLine1+booking.addressLine2;
	var uid = booking.user.id;
	fetch('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key='+$scope.gmapKey+'&components=country:ID')
    .then(function (response) {
        return response.json();
    })
    .then(function (data) {
        console.log(data);
        address = btoa(address);

        $http({
            method: 'POST',
            url: baseUrl + "/booking/getgeoloc/"+uid+"/"+address,
            data: data,
            headers: {
                "Content-Type": "application/json"              
            }
        }).success(function(response) {
        	
        	showLoader(false);
        	
        	// if(response.postalCode != ""){
    	    //     $scope.preOrderList[i].postalCode = response.postalCode;
    	    //     $scope.preOrderList[i].kecematan = response.kecematan;
    	    //     $scope.preOrderList[i].kelurahan = response.kelurahan;
    	    //     $scope.preOrderList[i].latitude = response.latitude;
    	    //     $scope.preOrderList[i].longitude = response.longitude;
    	    //     $scope.preOrderList[i].kota = response.kota;
    	    //     $scope.preOrderList[i].provinci = response.provinci;
    	    //     $scope.preOrderList[i].zoneId = response.zoneId;
    	        
    	    //     $scope.updatePreBooking(booking);
         //    }

             if(response.postalCode != ""){
                $scope.preOrderList[i].postalCode = response.postalCode;
                $scope.preOrderList[i].kecematan = response.kecematan;
                $scope.preOrderList[i].kelurahan = response.kelurahan;
                $scope.preOrderList[i].latitude = response.latitude;
                $scope.preOrderList[i].longitude = response.longitude;
                $scope.preOrderList[i].kota = response.kota;
                $scope.preOrderList[i].provinci = response.provinci;
                $scope.preOrderList[i].zoneId = response.zoneId;
                
                $scope.updatePreBooking(booking);
            }
            else{

                if(response.latitude != ""){
                    var latlng = response.latitude + "," + response.longitude;

                    var url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+latlng+'&key='+$scope.gmapKey;
                    
                    fetch(url)
                    .then(function (response) {
                        return response.json();
                    })

                    .then(function (data) {
                        console.log(url);
                        console.log(data);

                        address = btoa(address);

                        $http({
                            method: 'POST',
                            url: baseUrl + "/booking/getgeolocbylatlng/"+uid+"/"+address,
                            data: data,
                            headers: {
                                "Content-Type": "application/json"              
                            }
                        })
                        .success(function(response) {

                            console.log("response");
                            console.log(response);

                            if(response.postalCode != ""){
                                $scope.preOrderList[i].postalCode = response.postalCode;
                                $scope.preOrderList[i].kecematan = response.kecematan;
                                $scope.preOrderList[i].kelurahan = response.kelurahan;
                                $scope.preOrderList[i].latitude = response.latitude;
                                $scope.preOrderList[i].longitude = response.longitude;
                                $scope.preOrderList[i].kota = response.kota;
                                $scope.preOrderList[i].provinci = response.provinci;
                                $scope.preOrderList[i].zoneId = response.zoneId;
                                
                                $scope.updatePreBooking(booking);
                            }

                        })
                        .error(function(response) {
            
                        });

                    })
                    .catch(function (err) {
                        console.log("Something went wrong!", err);
                    });


            }

            }









            
            
        }).error(function(response) {
        	showLoader(false);
        	
        });
        
    })
    .catch(function (err) {
        console.log("Something went wrong!", err);
    });
	
	
}


$scope.generateAutoRoute = function(){
    $http({
        method: 'GET',
        url: baseUrl + "/zone/cron/generate-route",
        headers: {
            "Content-Type": "application/json"              
        }
    }).success(function(response) {
        $scope.genMsgObj = response;
        
    }).error(function(response) {
        $scope.genMsgObj = response;
       
    });
}



$scope.updateOrders = function(bookings){
	console.log(bookings);
	var noZoneList = bookings.filter(function (item) {
        return item.zoneId == 0;
    });
    var noPostalCodeList = bookings.filter(function (item) {
        return item.postalCode == null;
    }); 
    
    var noPickUpList = bookings.filter(function (item) {
    	console.log(item);
        return item.pickUpLocation.pickUpLatitude == null || item.pickUpLocation.pickUpLatitude == 0;
    });
    
    var validate = "";
	
	if(noZoneList.length > 0) validate += noZoneList.length+" zones are empty. ";
	if(noPostalCodeList.length > 0) validate += noPostalCodeList.length+" postal codes are empty.";
	if(noPickUpList.length > 0) validate += noPickUpList.length+" pickup location fields are empty.";
     
    if(validate != ""){
    	$.sweetModal({
            content: validate,
            icon: $.sweetModal.ICON_ERROR
        });
    	//alert(validate);
    	return false;
    } 
     
	
	showLoader(true);
	$http({
        method: 'POST',
        url: baseUrl + "/booking/update-bookings",
        data: bookings,
        headers: {
            "Content-Type": "application/json"              
        }
    }).success(function(response) {
    	showLoader(false);
        if(response.status=="success"){
	    	$scope.uoMsg = response.message;
	        $scope.msgColor = "success";

            //$scope.generateAutoRoute();
        }
        else{
        	$scope.uoMsg = response.message;
	        $scope.msgColor = "warning";
        }
        
    }).error(function(response) {
    	showLoader(false);
    	$scope.uoMsg = "Unwanted error";
        $scope.msgColor = "danger";
       
    });
	
	
	
	
}

$scope.removePreBooking = function(booking,index){
	
	if(!confirm("Are you sure?")){
		return false;
	}
	
	showLoader(true);
	$http({
        method: 'GET',
        url: baseUrl + "/booking/removepreorder/"+booking.pboId,
       
    }).success(function(response) {
        
        if(response){
        	$scope.getPreOrderList($scope.userlist); 
        }
        
    }).error(function(response) {
        showLoader(false);
        console.log(response);
       
    });

}


$scope.generatePickUpLatitudeLongitude = function(pickup,i){
	
	var uid = pickup.user.id;
	$scope.showLatLng = true;
	showLoader(true);
	
	console.log("Index: "+i);
	
	var address = pickup.pickUpLocation;
	if(pickup.pickUpKecematan != null && pickup.pickUpKecematan != "")
		address += ",+"+pickup.pickUpKecematan;
	if(pickup.pickUpKelurahan != null && pickup.pickUpKelurahan != "")
		address += ",+"+pickup.pickUpKelurahan;
	if(pickup.pickUpKota != null && pickup.pickUpKota != "")
		address += ",+"+pickup.pickUpKota;
	if(pickup.pickUpProvinsi != null && pickup.pickUpProvinsi != "")
		address += ",+"+ pickup.pickUpProvinsi;
	
	var url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key='+$scope.gmapKey+'&components=country:ID';
	
	fetch(url)
    .then(function (response) {
        return response.json();
    })
    .then(function (data) {
    	
        
        address = btoa(address);

        $http({
            method: 'POST',
            url: baseUrl + "/booking/getgeoloc/"+uid+"/"+address,
            data: data,
            headers: {
                "Content-Type": "application/json"              
            }
        }).success(function(response) {
        	
        	showLoader(false);
        	console.log(response);
        	$scope.pickUpLocation.pickPinCode = $scope.preOrderList[i].pickUpLocation.pickPinCode = response.postalCode;
	        $scope.pickUpLocation.pickUpKecematan = $scope.preOrderList[i].pickUpLocation.pickUpKecematan = response.kecematan;
	        $scope.pickUpLocation.pickUpKelurahan = $scope.preOrderList[i].pickUpLocation.pickUpKelurahan = response.kelurahan;
	        $scope.pickUpLocation.pickUpLatitude = $scope.preOrderList[i].pickUpLocation.pickUpLatitude = response.latitude;
	        $scope.pickUpLocation.pickUpLongitude = $scope.preOrderList[i].pickUpLocation.pickUpLongitude = response.longitude;
	        $scope.pickUpLocation.pickUpKota = $scope.preOrderList[i].pickUpLocation.pickUpKota = response.kota;
	        $scope.pickUpLocation.pickUpProvinsi = $scope.preOrderList[i].pickUpLocation.pickUpProvinsi = response.provinci;
        	$scope.showLatLng = false;
        	
            
        }).error(function(response) {
        	showLoader(false);
        	$scope.showLatLng = false;
        	
        });
        
    })
    .catch(function (err) {
        console.log("Something went wrong!", err);
        $scope.showLatLng = false;
    });
	
	
}

$scope.updatePickUpLocationDetails = function(pickUpLocation){
	console.log(pickUpLocation);
	
	showLoader(true);
	$http({
        method: 'POST',
        url: baseUrl + "/pickUp/updateLocation",
        data: pickUpLocation,
        headers: {
            "Content-Type": "application/json"              
        }
    }).success(function(response) {
    	showLoader(false);
    	console.log(response);
    	$scope.getPreOrderList($scope.userlist); 
    	alert("Pickup location updated");
    	$("#viewPickUpModal").modal("hide");
        
    }).error(function(response) {
    	showLoader(false);

        $.sweetModal({
            content: "Error! Something went wrong",
            icon: $.sweetModal.ICON_ERROR
        });
       
    });
	
	
	
	
	
}

$scope.getPickUpData = function(pickUp,i){
	console.log(pickUp);
	$scope.index = i;
	$scope.pickUpLocation = pickUp;
	
}

$scope.getDownloadLink = function(){
        showLoader(true);

        $http({
          url: baseUrl + "/booking/download-bookings/"+$scope.selCId+"/from/"+$scope.sDate+"/to/"+$scope.eDate,
          method: 'GET',
          transformResponse: [function (response) {
              // Data response is plain text at this point
              // So just return it, or do your parsing here
              $scope.downloadLink = "";
              if(response != "No"){
                $scope.download = false;
                $scope.downloadLink = response;
              }
              showLoader(false);
              return response;
          }]
        });

    }


$scope.getPastOrders = function(sDate,eDate) {
     $scope.download = true;  
     $scope.downloadLink = ""; 
     $scope.sDate = sDate;
     $scope.eDate = eDate;
     $scope.getOrderList(0);
     console.log(sDate);
     console.log(eDate);
}




	
});







app.controller('InsideCtrl', function($scope, $http, $localStorage) {
	
	var vm = this;	
	var url = baseUrl + "/zone/driverzone/"+$scope.driver.id;
	console.log(url);
	$http.get(url)
	   .then(function(response){

		   vm.zonename = response.data.zoneName;
		   vm.zoneid = response.data.zoneId;
		   console.log(response.data);
		   
	   }).finally(function(){

	   });	
});

app.controller('OrderInsideCtrl', function($scope, $http, $localStorage) {
	
	var vm = this;	
	var url = baseUrl + "/zone/driverzone/"+$scope.driver.id;
	console.log(url);
	$http.get(url)
	   .then(function(response){

		   vm.zonename = response.data.zoneName;
		   vm.zoneid = response.data.zoneId;
		   console.log(response.data);
		   
	   }).finally(function(){

	   });	
});

app.filter('range', function() {
	  return function(input, total) {
	    total = parseInt(total);
	    for (var i=0; i<total; i++)
	      input.push(i);
	    return input;
	  };
	});