// LiveOrder Controller
app.controller('liveOrderCtrl', function($scope, $http, $localStorage, $interval,NgMap) {
    console.log("Live Order Controller");

    $scope.user=$localStorage.user;
    NgMap.getMap({id:'ngmappickup'}).then(function(map) {
        console.log('map', map);
        $scope.map = map;
    });

    NgMap.getMap({id:'ngmaporderlist'}).then(function(map1) {
        console.log('map1', map1);
        $scope.map1 = map1;
    });

    $scope.deliveryInit = function(){
        $scope.download = false;
        $scope.downloadLink = "";
    }

    $scope.showPinDetail=function(event,selPoint){
        console.log("Pin Detail");
        $scope.sPoint=selPoint;
        console.log($scope.sPoint);
        $scope.map.showInfoWindow('myInfoWindow', this);
    }

    $scope.showPinDetailOrder=function(event,selPoint){
        console.log("Pin Detail");
        $scope.sOrder=selPoint;
        $scope.map1.showInfoWindow('myOrderInfoWindow', this);
    }
    
    $scope.currentPage = 0;
    $scope.pageSize = 10;
    $scope.tempBookingList = [];
    $scope.q = '';
    
    $scope.getData = function () {
        $scope.tempBookingList=[];
        angular.forEach($scope.bookingList, function(value, key) {
            if(( value.pmid && value.pmid.toLowerCase().includes($scope.q.toLowerCase())) || value.reciverName.toLowerCase().includes($scope.q.toLowerCase()) ||
            value.reciverContactNo.includes($scope.q.toLowerCase()) || value.dropLocation.toLowerCase().includes($scope.q.toLowerCase()) ||
            value.deliveryType.toLowerCase().includes($scope.q.toLowerCase()) || value.dropPinCode.includes($scope.q)) {

                $scope.tempBookingList.push(value);
            }
        });  
        $scope.currentPage=0;
        $scope.numberOfPages=  Math.ceil($scope.tempBookingList.length/$scope.pageSize); 
      return $scope.tempBookingList;
     
    }
    
    // $scope.numberOfPages=function(){
    //     return Math.ceil($scope.getData().length/$scope.pageSize);                
    // }
    


    $scope.bookingList=[];
    $scope.rabbitList=[];

    $scope.getMapMarkers = function() {
      
        console.log("get All Marker");
        $http({
            method: 'GET',
            url: "/booking/rabbitsCurrentLocation",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log("response of all markers");
            console.log(response);
            $scope.rabbitLocation=response.data;
           
        }).error(function(response) {
            console.log("response erro of all markers");
            console.log(response);
           


        });
    }

    // Fetch Live Progress bar data 
    $scope.getLiveOrdersByUser = function() {
        console.log("get All picup");
        console.log($scope.companyName);
        showLoader(true);
        
        $http({
            method: 'GET',
            url: baseUrl + "/booking/getLiveOrderData/" + $scope.selCId,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                // $.sweetModal({
                //     content: "Error",
                //     icon: $.sweetModal.ICON_ERROR
                // });
            } else {

                $scope.orderStats = response.data.floatLiveOrder;
                $scope.dataOrder = response.data.chartData;
                console.log($scope.dataOrder);
                
                var maxGraphVal=0;
                for(var k=0; k<$scope.dataOrder.length; k++){
                    var notPicVal = parseInt($scope.dataOrder[k].notPicked);
                    var picVal = parseInt($scope.dataOrder[k].picked);
                    var delVal = parseInt($scope.dataOrder[k].delivered);

                    if(k==0){
                        maxGraphVal=notPicVal+picVal+delVal;
                    }
                    if((notPicVal+picVal+delVal)>maxGraphVal){
                        maxGraphVal=notPicVal+picVal+delVal;
                    }
                }


                for (var i = 0; i < $scope.dataOrder.length; i++) {
                    var notPicVal = parseInt($scope.dataOrder[i].notPicked);
                    var picVal = parseInt($scope.dataOrder[i].picked);
                    var delVal = parseInt($scope.dataOrder[i].delivered);
                    if ((notPicVal + picVal + delVal) == 0) {
                        $scope.dataOrder[i].notPicked = 0;
                        $scope.dataOrder[i].picked = 0;
                        $scope.dataOrder[i].delivered = 0;
                    } else {
                        
                        $scope.dataOrder[i].notPicked = (notPicVal + picVal + delVal)*100/parseInt(maxGraphVal);
                        $scope.dataOrder[i].picked = (picVal + delVal) * 100 / parseInt(maxGraphVal);
                        $scope.dataOrder[i].delivered = (delVal) * 100 / parseInt(maxGraphVal);
                    }


                }
                console.log($scope.dataOrder);
                showLoader(false);
              
            }
        }).error(function(response) {
            console.log(response);
            console.log("Error occrured");
            if(response.error=="invalid_token"){
                $.sweetModal({
                    content: 'Your Session Expired ! Please login again',
                    icon: $.sweetModal.ICON_WARNING,
                });
                 $interval(function() {
                    $localStorage.$reset();
                    location.href = "login.html";
                }, 2000);
               
               
            }
            showLoader(false);
        

        });
    }



    $scope.getLiveOrdersByBusiness = function(bid) {
        console.log("get All picup");
        //$scope.companyName = companyName;
        $scope.selCId = bid;
        //console.log($scope.companyName);
        showLoader(true);

        $scope.getBusinessList();
        
        $http({
            method: 'GET',
            url: baseUrl + "/booking/getLiveOrderData/" + $scope.selCId,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                // $.sweetModal({
                //     content: "Error",
                //     icon: $.sweetModal.ICON_ERROR
                // });
            } else {

                $scope.orderStats = response.data.floatLiveOrder;
                $scope.dataOrder = response.data.chartData;
                console.log($scope.dataOrder);
                
                var maxGraphVal=0;
                for(var k=0; k<$scope.dataOrder.length; k++){
                    var notPicVal = parseInt($scope.dataOrder[k].notPicked);
                    var picVal = parseInt($scope.dataOrder[k].picked);
                    var delVal = parseInt($scope.dataOrder[k].delivered);

                    if(k==0){
                        maxGraphVal=notPicVal+picVal+delVal;
                    }
                    if((notPicVal+picVal+delVal)>maxGraphVal){
                        maxGraphVal=notPicVal+picVal+delVal;
                    }
                }


                for (var i = 0; i < $scope.dataOrder.length; i++) {
                    var notPicVal = parseInt($scope.dataOrder[i].notPicked);
                    var picVal = parseInt($scope.dataOrder[i].picked);
                    var delVal = parseInt($scope.dataOrder[i].delivered);
                    if ((notPicVal + picVal + delVal) == 0) {
                        $scope.dataOrder[i].notPicked = 0;
                        $scope.dataOrder[i].picked = 0;
                        $scope.dataOrder[i].delivered = 0;
                    } else {
                        
                        $scope.dataOrder[i].notPicked = (notPicVal + picVal + delVal)*100/parseInt(maxGraphVal);
                        $scope.dataOrder[i].picked = (picVal + delVal) * 100 / parseInt(maxGraphVal);
                        $scope.dataOrder[i].delivered = (delVal) * 100 / parseInt(maxGraphVal);
                    }


                }
                console.log($scope.dataOrder);
                showLoader(false);
              
            }
        }).error(function(response) {
            console.log(response);
            console.log("Error occrured");
            if(response.error=="invalid_token"){
                $.sweetModal({
                    content: 'Your Session Expired ! Please login again',
                    icon: $.sweetModal.ICON_WARNING,
                });
                 $interval(function() {
                    $localStorage.$reset();
                    location.href = "login.html";
                }, 2000);
               
               
            }
            showLoader(false);
        

        });
    }

   

    
    $scope.getDownloadLink = function(){
        showLoader(true);

        $http({
          url: baseUrl + "/booking/download-bookings/"+$scope.selCId+"/from/"+$scope.sDate+"/to/"+$scope.eDate,
          method: 'GET',
          transformResponse: [function (response) {
              // Data response is plain text at this point
              // So just return it, or do your parsing here
              $scope.downloadLink = "";
              if(response != "No"){
                $scope.download = false;
                $scope.downloadLink = response;
              }
              showLoader(false);
              return response;
          }]
        });

    }



    //get All Booking by User
    $scope.getAllPickUpLocByUser = function(orderStatus) {
        showLoader(true);
        console.log("get All Booking");

var today = new Date();
var date = today.getDate()+'-'+(today.getMonth()+1)+'-'+today.getFullYear();
$scope.sDate = date;
$scope.eDate = date;

        $http({
            method: 'GET',
            url: baseUrl + "/booking/getBookingOrder/" + $scope.selCId + "/status/" + orderStatus,
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.bookingList = response.data;
                $scope.tempBookingList=response.data;
                $scope.currentPage=0;
                $scope.numberOfPages=  Math.ceil($scope.tempBookingList.length/$scope.pageSize); 
               
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });

        });
    };
   

    $scope.getCompanyData=function(cmp){
        console.log(cmp);
        $scope.selCId=cmp.id;
        $scope.getLiveOrdersByUser();
        $scope.getMapMarkers();
        $scope.getAllPickUpLocByUser("all");

    }

   var initc={id:0};

    $scope.getCompanyData(initc);

   $interval(function() {
        $scope.getLiveOrdersByUser();
        $scope.getMapMarkers();
        $scope.getAllPickUpLocByUser("all");
    }, 10000);


    $scope.getCompanyList=function(){

        showLoader(true);
        console.log("get Company List");
        $http({
            method: 'GET',
            url: baseUrl + "/api/companyList",
            headers: {
                "Content-Type": "application/json",
                "Authorization": "Bearer " + $localStorage.token
            }
        }).success(function(response) {
            console.log(response);
            if (response.status == "Error") {
                $.sweetModal({
                    content: "Error",
                    icon: $.sweetModal.ICON_ERROR
                });
            } else {
                $scope.companyList = response;
                
                showLoader(false);
                /* $.sweetModal({
                     content: "Success",
                     icon: $.sweetModal.ICON_SUCCESS
                 });*/
            }
        }).error(function(response) {
            console.log(response);
            showLoader(false);
            $.sweetModal({
                content: "Error",
                icon: $.sweetModal.ICON_ERROR
            });

        });        
    }

    $scope.getBusinessList=function(){

        showLoader(true);
        console.log("get Business List"+$scope.selCId);
        $http({
            method: 'GET',
            url: baseUrl + "/booking/pickup-list/"+$scope.selCId,
            headers: {
                "Content-Type": "application/json"
            }
        }).success(function(response) {
            console.log(response);
           $scope.businessList = response;
        });        
    }


    $scope.getCompanyList();

});

